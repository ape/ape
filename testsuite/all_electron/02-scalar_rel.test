
Test     : All-electron: scalar-relativistic Cr
Programs : ape
TestGroups : all; ae
Enabled  : Yes


Input: 02-scalar_rel.01-no_spin.inp

### The energies and eigenvalues are from the NIST database ###
# Etot  = -1047.723197
# Ekin  =  1056.270005
# Ecoul =   443.512785
# Eenuc = -2501.774322
# Exc   =   -45.731664
#
# 1s = -215.070201
# 2s =  -24.411958
# 2p =  -20.570871
# 3s =   -2.697883
# 3p =   -1.664941
# 4s =   -0.153632
# 3d =   -0.115300

#Energies
match ; Etot  ; GREP(ae/info, 'Energies', 40, 1) ; -1047.723197; 3e-04 # Ref. should not be updated
match ; Ekin  ; GREP(ae/info, 'Energies', 40, 2) ; 1056.270005; 8e-04 # Ref. should not be updated
match ; Ecoul ; GREP(ae/info, 'Energies', 40, 3) ; 443.512785; 2e-05 # Ref. should not be updated
match ; Eenuc ; GREP(ae/info, 'Energies', 40, 4) ; -2501.774322; 10e-04 # Ref. should not be updated
match ; Exc   ; GREP(ae/info, 'Energies', 40, 5) ; -45.731664; 5e-06 # Ref. should not be updated

#Eigenvalues
match ; 1s Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 2) ; -215.070201; 9e-05 # Ref. should not be updated
match ; 2s Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 3) ; -24.411958; 8e-06 # Ref. should not be updated
match ; 2p Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 4) ; -20.570871; 2e-06 # Ref. should not be updated
match ; 3s Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 5) ; -2.697883; 3e-06 # Ref. should not be updated
match ; 3p Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 6) ; -1.664941; 10e-07 # Ref. should not be updated
match ; 4s Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 7) ; -0.153632; 3e-06 # Ref. should not be updated
match ; 3d Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 8) ; -0.115300; 1e-12 # Ref. should not be updated

#Wavefunctions
match ; 1s Wavefunction            ; LINE(ae/wf-1s, 467, 20) ; 5.86275354E-01; 2e-07
match ; 1s Wavefunction Derivative ; LINE(ae/wf-1s, 467, 39) ; -1.32166261E+01; 5e-06
match ; 2s Wavefunction            ; LINE(ae/wf-2s, 467, 20) ; 7.48944156E+00; 5e-07
match ; 2s Wavefunction Derivative ; LINE(ae/wf-2s, 467, 39) ; -3.38077419E+01; 9e-06
match ; 2p Wavefunction            ; LINE(ae/wf-2p, 467, 20) ; 6.91986978E+00; 10e-09
match ; 2p Wavefunction Derivative ; LINE(ae/wf-2p, 467, 39) ; -4.07215904E+01; 4e-07
match ; 3s Wavefunction            ; LINE(ae/wf-3s, 467, 20) ; -1.98183032E+00; 5e-08
match ; 3s Wavefunction Derivative ; LINE(ae/wf-3s, 467, 39) ; 2.14417092E+01; 2e-06
match ; 3p Wavefunction            ; LINE(ae/wf-3p, 467, 20) ; -1.25191593E+00; 1e-12
match ; 3p Wavefunction Derivative ; LINE(ae/wf-3p, 467, 39) ; 2.05890416E+01; 1e-07
match ; 4s Wavefunction            ; LINE(ae/wf-4s, 467, 20) ; 4.65203930E-01; 3e-08
match ; 4s Wavefunction Derivative ; LINE(ae/wf-4s, 467, 39) ; -5.49499820E+00; 5e-07
match ; 3d Wavefunction            ; LINE(ae/wf-3d, 467, 20) ; 1.15542665E+00; 2e-08
match ; 3d Wavefunction Derivative ; LINE(ae/wf-3d, 467, 39) ; 2.60249979E+00; 1e-12

#Vhxc
match ; v_hxc ; LINE(ae/v_hxc, 467, 20) ; 4.70287804E+01; 1e-12
match ; v_c   ; LINE(ae/v_c,   467, 20) ; -1.12103447E-01; 1e-12
match ; v_x   ; LINE(ae/v_x,   467, 20) ; -3.16601325E+00; 4e-08

#Density
match ; density       ; LINE(ae/density, 467, 20) ; 3.37670310E+01; 2e-06
match ; grad. density ; LINE(ae/density, 467, 38) ; -3.88304870E+02; 3e-05
match ; lapl. density ; LINE(ae/density, 467, 56) ; 6.65279231E+02; 5e-04
match ; tau           ; LINE(ae/tau,     467, 20) ; 2.15689495E+03; 2e-05


Input: 02-scalar_rel.02-spin.inp

#Energies
match ; Etot  ; GREP(ae/info, 'Energies', 40, 1) ; -1047.910722; 2e-04
match ; Ekin  ; GREP(ae/info, 'Energies', 40, 2) ; 1056.485884; 3e-04
match ; Ecoul ; GREP(ae/info, 'Energies', 40, 3) ; 444.530866; 3e-05
match ; Eenuc ; GREP(ae/info, 'Energies', 40, 4) ; -2502.954324; 8e-05
match ; Exc   ; GREP(ae/info, 'Energies', 40, 5) ; -45.973149; 10e-07

#Eigenvalues
match ; 1s_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 2) ; -215.02112; 4e-05
match ; 1s_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 3) ; -215.02010; 2e-05
match ; 2s_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 4) ; -24.39143; 1e-12
match ; 2s_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 5) ; -24.32809; 10e-06
match ; 2p_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 6) ; -20.54367; 10e-06
match ; 2p_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 7) ; -20.49446; 10e-06
match ; 3s_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 8) ; -2.72684; 10e-06
match ; 3s_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 9) ; -2.57191; 2e-05
match ; 3p_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 10) ; -1.69381; 2e-05
match ; 3p_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 11) ; -1.54106; 2e-05
match ; 4s_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 12) ; -0.17004; 2e-05
match ; 3d_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 13) ; -0.14325; 1e-05
match ; 4s_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 14) ; -0.09606; 2e-05
match ; 3d_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 15) ; -0.00886; 9e-03

#Wavefunctions
match ; 1s_up Wavefunction            ; LINE(ae/wf-1s_up, 467, 20) ;  5.86218338E-01; 2e-07
match ; 1s_up Wavefunction Derivative ; LINE(ae/wf-1s_up, 467, 39) ; -1.32160894E+01; 3e-06
match ; 1s_dn Wavefunction            ; LINE(ae/wf-1s_dn, 467, 20) ;  5.86312689E-01; 2e-07
match ; 1s_dn Wavefunction Derivative ; LINE(ae/wf-1s_dn, 467, 39) ; -1.32168693E+01; 5e-06
match ; 2s_up Wavefunction            ; LINE(ae/wf-2s_up, 467, 20) ;  7.49315618E+00; 2e-06
match ; 2s_up Wavefunction Derivative ; LINE(ae/wf-2s_up, 467, 39) ; -3.38673900E+01; 4e-05
match ; 2s_dn Wavefunction            ; LINE(ae/wf-2s_dn, 467, 20) ;  7.48603130E+00; 2e-06
match ; 2s_dn Wavefunction Derivative ; LINE(ae/wf-2s_dn, 467, 39) ; -3.37533218E+01; 4e-05
match ; 2p_up Wavefunction            ; LINE(ae/wf-2p_up, 467, 20) ;  6.92200706E+00; 2e-06
match ; 2p_up Wavefunction Derivative ; LINE(ae/wf-2p_up, 467, 39) ; -4.07819913E+01; 4e-05
match ; 2p_dn Wavefunction            ; LINE(ae/wf-2p_dn, 467, 20) ;  6.91787678E+00; 6e-07
match ; 2p_dn Wavefunction Derivative ; LINE(ae/wf-2p_dn, 467, 39) ; -4.06664715E+01; 3e-05
match ; 3s_up Wavefunction            ; LINE(ae/wf-3s_up, 467, 20) ; -1.97286738E+00; 5e-06
match ; 3s_up Wavefunction Derivative ; LINE(ae/wf-3s_up, 467, 39) ;  2.13955159E+01; 5e-05
match ; 3s_dn Wavefunction            ; LINE(ae/wf-3s_dn, 467, 20) ; -1.98582841E+00; 6e-06
match ; 3s_dn Wavefunction Derivative ; LINE(ae/wf-3s_dn, 467, 39) ;  2.14303035E+01; 5e-05
match ; 3p_up Wavefunction            ; LINE(ae/wf-3p_up, 467, 20) ; -1.24216844E+00; 4e-06
match ; 3p_up Wavefunction Derivative ; LINE(ae/wf-3p_up, 467, 39) ;  2.05065995E+01; 4e-05
match ; 3p_dn Wavefunction            ; LINE(ae/wf-3p_dn, 467, 20) ; -1.25696183E+00; 5e-06
match ; 3p_dn Wavefunction Derivative ; LINE(ae/wf-3p_dn, 467, 39) ;  2.05863444E+01; 6e-05
match ; 4s_up Wavefunction            ; LINE(ae/wf-4s_up, 467, 20) ;  4.03975644E-01; 1e-05
match ; 4s_up Wavefunction Derivative ; LINE(ae/wf-4s_up, 467, 39) ; -4.77178706E+00; 2e-04
match ; 4s_dn Wavefunction            ; LINE(ae/wf-4s_dn, 467, 20) ;  4.83203094E-01; 4e-06
match ; 4s_dn Wavefunction Derivative ; LINE(ae/wf-4s_dn, 467, 39) ; -5.69570729E+00; 5e-05
match ; 3d_up Wavefunction            ; LINE(ae/wf-3d_up, 467, 20) ;  1.04863377E+00; 2e+00
match ; 3d_up Wavefunction Derivative ; LINE(ae/wf-3d_up, 467, 39) ;  2.35655862E+00; 3e+00
match ; 3d_dn Wavefunction            ; LINE(ae/wf-3d_dn, 467, 20) ;  1.16303860E+00; 3e-06
match ; 3d_dn Wavefunction Derivative ; LINE(ae/wf-3d_dn, 467, 39) ;  2.62884481E+00; 6e-06

#Potentials
match ; v_hxc_up ; LINE(ae/v_hxc_up, 467, 20) ; 4.70966755E+01; 5e-06
match ; v_hxc_dn ; LINE(ae/v_hxc_dn, 467, 20) ; 4.70645023E+01; 5e-06
match ; v_c_up   ; LINE(ae/v_c_up,   467, 20) ; -1.13168892E-01; 2e-08
match ; v_c_dn   ; LINE(ae/v_c_dn,   467, 20) ; -1.11056470E-01; 2e-08
match ; v_x_up   ; LINE(ae/v_x_up,   467, 20) ; -3.14892840E+00; 2e-07
match ; v_x_dn   ; LINE(ae/v_x_dn,   467, 20) ; -3.18321404E+00; 9e-07

#Densities
match ; density_up       ; LINE(ae/density_up, 467, 20) ;  1.66121743E+01; 3e-06
match ; density_dn       ; LINE(ae/density_dn, 467, 38) ; -1.92904569E+02; 3e-05
match ; grad. density_up ; LINE(ae/density_up, 467, 56) ;  3.30778943E+02; 4e-04
match ; grad. density_dn ; LINE(ae/density_dn, 467, 20) ;  1.71597388E+01; 4e-06
match ; lapl. density_up ; LINE(ae/density_up, 467, 38) ; -1.95287781E+02; 2e-04
match ; lapl. density_dn ; LINE(ae/density_dn, 467, 56) ;  3.33589822E+02; 9e-04
match ; tau_up           ; LINE(ae/tau_up,     467, 20) ;  1.05202180E+03; 5e-04
match ; tau_dn           ; LINE(ae/tau_dn,     467, 20) ;  1.10473949E+03; 6e-04

