
Test     : Pseudopotentials: fully-relativistic Xe
Programs : ape
TestGroups : all; ps
Enabled  : Yes

Input: 03-rel.01-ae.inp

Input: 03-rel.02-hamann.inp

#Coefficient
match; 5s0.5 cl coefficient; GREP(pp/info, 'State: 5s0.5', 11, 3); 4.2778585806; 3e-06
match; 5p0.5 cl coefficient; GREP(pp/info, 'State: 5p0.5', 11, 3); 4.7178668716; 3e-06
match; 5p1.5 cl coefficient; GREP(pp/info, 'State: 5p1.5', 11, 3); 5.9665297991; 4e-06

#Pseudo-Wavefunctions
match; 5s0.5 Large Wavefunction (small r);        LINE(pp/wf-5s0.5,  836, 20); 2.21257564E-01; 7e-07
match; 5s0.5 Large Wavefunction (large r);        LINE(pp/wf-5s0.5, 1117, 20); 1.01132968E-05; 2e-10
match; 5s0.5 Small Wavefunction (small r);        LINE(pp/wf-5s0.5,  836, 38); 1.52239418E-03; 7e-09
match; 5s0.5 Small Wavefunction (large r);        LINE(pp/wf-5s0.5, 1117, 38); -4.81706377E-08; 1e-12
match; 5s0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 56); 4.17183558E-01; 2e-06
match; 5s0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 56); -1.32019679E-05; 2e-10
match; 5s0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 74); 2.93989160E-03; 3e-08
match; 5s0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 74); 6.31726619E-08; 1e-12

match; 5p0.5 Large Wavefunction (small r);        LINE(pp/wf-5p0.5,  836, 20); 1.31101835E-01; 9e-07
match; 5p0.5 Large Wavefunction (large r);        LINE(pp/wf-5p0.5, 1117, 20); 1.87502803E-04; 2e-09
match; 5p0.5 Small Wavefunction (small r);        LINE(pp/wf-5p0.5,  836, 38); 3.18867504E-03; 7e-08
match; 5p0.5 Small Wavefunction (large r);        LINE(pp/wf-5p0.5, 1117, 38); -4.99902679E-07; 3e-12
match; 5p0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5p0.5,  836, 56); 3.50321993E-01; 4e-06
match; 5p0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5p0.5, 1117, 56); -1.74450870E-04; 9e-10
match; 5p0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5p0.5,  836, 74); 2.93453941E-03; 4e-07
match; 5p0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5p0.5, 1117, 74); 4.57760864E-07; 3e-12

match; 5p1.5 Large Wavefunction (small r);        LINE(pp/wf-5p1.5,  836, 20); 1.11921383E-01; 7e-07
match; 5p1.5 Large Wavefunction (large r);        LINE(pp/wf-5p1.5, 1117, 20); 3.04049024E-04; 2e-09
match; 5p1.5 Small Wavefunction (small r);        LINE(pp/wf-5p1.5,  836, 38); 2.82024695E-04; 5e-09
match; 5p1.5 Small Wavefunction (large r);        LINE(pp/wf-5p1.5, 1117, 38); -1.07881409E-06; 7e-12
match; 5p1.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5p1.5,  836, 56); 3.00744925E-01; 8e-06
match; 5p1.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5p1.5, 1117, 56); -2.65312414E-04; 2e-09
match; 5p1.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5p1.5,  836, 74); 9.16507652E-04; 2e-08
match; 5p1.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5p1.5, 1117, 74); 9.62613815E-07; 6e-12

#Pseudopotentials
match; s0.5 Pseudopotential (small r); LINE(pp/pp-s0.5,  836, 20); 1.01800084E+00; 2e-05
match; s0.5 Pseudopotential (large r); LINE(pp/pp-s0.5, 1117, 20); -7.98767332E-01; 5e-08
match; p0.5 Pseudopotential (small r); LINE(pp/pp-p0.5,  836, 20); -1.10955835E+00; 2e-05
match; p0.5 Pseudopotential (large r); LINE(pp/pp-p0.5, 1117, 20); -7.98767332E-01; 5e-08
match; p1.5 Pseudopotential (small r); LINE(pp/pp-p1.5,  836, 20); -9.39976249E-01; 2e-05
match; p1.5 Pseudopotential (large r); LINE(pp/pp-p1.5, 1117, 20); -7.98767332E-01; 5e-08

# Self-Consistency tests
match; 5s0.5 Norm test ; GREP(pp/info, 'Self-Consistency', 27, 2); 0.9999957; 1e-12
match; 5s0.5 Slope test; GREP(pp/info, 'Self-Consistency', 39, 2); 1.0000563; 2e-07
match; 5p0.5 Norm test ; GREP(pp/info, 'Self-Consistency', 27, 3); 1.0000005; 1e-12
match; 5p0.5 Slope test; GREP(pp/info, 'Self-Consistency', 39, 3); 1.0000582; 6e-06
match; 5p1.5 Norm test ; GREP(pp/info, 'Self-Consistency', 27, 4); 0.9999977; 1e-12
match; 5p1.5 Slope test; GREP(pp/info, 'Self-Consistency', 39, 4); 1.0000081; 2e-07

#KB  projectors
match; Vanderbilt function v0 ; GREP(kb/info, "Vanderbilt function", 20, 2) ; -6.250497; 1e-12
match; Vanderbilt function v1 ; GREP(kb/info, "Vanderbilt function", 32, 2) ; -0.146398; 1e-12
match; Vanderbilt function v2 ; GREP(kb/info, "Vanderbilt function", 44, 2) ; 0.007144; 1e-12
match; Vanderbilt function v3 ; GREP(kb/info, "Vanderbilt function", 56, 2) ; -0.000155; 1e-12
match; 5s0.5 KB Energy ; GREP(kb/info, "KB Energy", 13, 1) ; -6.8841; 5e-04
match; 5p0.5 KB Energy ; GREP(kb/info, "KB Energy", 13, 2) ; -4.4654; 8e-04
match; 5p1.5 KB Energy ; GREP(kb/info, "KB Energy", 13, 3) ; -2.3608; 2e-04
match; 5s0.5 KB Cosine ; GREP(kb/info, "KB Energy", 28, 1) ; -0.1160; 1e-12
match; 5p0.5 KB Cosine ; GREP(kb/info, "KB Energy", 28, 2) ; -0.0889; 1e-12
match; 5p1.5 KB Cosine ; GREP(kb/info, "KB Energy", 28, 3) ; -0.1792; 1e-12
match; Local component ; LINE(kb/kb-local, 98, 20) ; -6.25049685E+00; 1e-12
match; s0.5 component  ; LINE(kb/kb-s0.5,  98, 20) ; 1.69592908E+00; 4e-06
match; p0.5 component  ; LINE(kb/kb-p0.5,  98, 20) ; 6.67335962E-04; 3e-09
match; p1.5 component  ; LINE(kb/kb-p1.5,  98, 20) ; 6.13101269E-04; 2e-09

#KB tests
match; 5s0.5 Local potential E0  ; GREP(kb/info, "Ghost state analysis", 35, 3)  ; -0.9875; 1e-12
match; 5s0.5 Local potential E1  ; GREP(kb/info, "Ghost state analysis", 51, 3)  ; -0.0794; 1e-12
match; 5p0.5 Local potential E0  ; GREP(kb/info, "Ghost state analysis", 35, 7)  ; -0.3071; 1e-12
match; 5p0.5 Local potential E1  ; GREP(kb/info, "Ghost state analysis", 51, 7)  ; 0.0000; 1e-12
match; 5p1.5 Local potential E0  ; GREP(kb/info, "Ghost state analysis", 35, 11) ; -0.3071; 1e-12
match; 5p1.5 Local potential E1  ; GREP(kb/info, "Ghost state analysis", 51, 11) ; 0.0000; 1e-12
match; Local localization radius ; GREP(kb/info, "Localization", 11, 1) ; 2.82; 1e-12
match; s0.5 localization radius  ; GREP(kb/info, "Localization", 11, 2) ; 2.67; 1e-12
match; p0.5 localization radius  ; GREP(kb/info, "Localization", 11, 3) ; 2.79; 1e-12
match; p1.5 localization radius  ; GREP(kb/info, "Localization", 11, 4) ; 2.76; 1e-12


Input: 03-rel.03-tm.inp

#TM coefficients
match; 5s0.5 c0  Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 4); -1.646258592E+00; 10e-06
match; 5s0.5 c2  Coefficient; GREP(pp/info, 'State: 5s0.5', 37, 4); 1.301503717E+00; 2e-05
match; 5s0.5 c4  Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 5); -3.387823849E-01; 1e-05
match; 5s0.5 c6  Coefficient; GREP(pp/info, 'State: 5s0.5', 37, 5); -1.209735324E-01; 2e-06
match; 5s0.5 c8  Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 6); 7.489861497E-02; 5e-07
match; 5s0.5 c10 Coefficient; GREP(pp/info, 'State: 5s0.5', 37, 6); -1.402822339E-02; 2e-07
match; 5s0.5 c12 Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 7); 9.351923922E-04; 2e-08
match; 5p0.5 c0  Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 4); -1.187441714E+00; 8e-06
match; 5p0.5 c2  Coefficient; GREP(pp/info, 'State: 5p0.5', 37, 4); 2.048774156E-01; 9e-06
match; 5p0.5 c4  Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 5); -5.996393626E-03; 5e-07
match; 5p0.5 c6  Coefficient; GREP(pp/info, 'State: 5p0.5', 37, 5); -9.154652264E-02; 3e-06
match; 5p0.5 c8  Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 6); 3.321068898E-02; 1e-06
match; 5p0.5 c10 Coefficient; GREP(pp/info, 'State: 5p0.5', 37, 6); -4.808913162E-03; 2e-07
match; 5p0.5 c12 Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 7); 2.601108093E-04; 1e-08
match; 5p1.5 c0  Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 4); -1.273923855E+00; 9e-06
match; 5p1.5 c2  Coefficient; GREP(pp/info, 'State: 5p1.5', 37, 4); 1.563677718E-01; 8e-06
match; 5p1.5 c4  Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 5); -3.492982876E-03; 4e-07
match; 5p1.5 c6  Coefficient; GREP(pp/info, 'State: 5p1.5', 37, 5); -6.320052237E-02; 2e-06
match; 5p1.5 c8  Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 6); 2.036807744E-02; 5e-07
match; 5p1.5 c10 Coefficient; GREP(pp/info, 'State: 5p1.5', 37, 6); -2.611834863E-03; 7e-08
match; 5p1.5 c12 Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 7); 1.247301982E-04; 4e-09

#Pseudo-Wavefunctions
match; 5s0.5 Large Wavefunction (small r)       ; LINE(pp/wf-5s0.5,  836, 20); 2.61128262E-01; 2e-06
match; 5s0.5 Large Wavefunction (large r)       ; LINE(pp/wf-5s0.5, 1117, 20); 1.01127542E-05; 2e-10
match; 5s0.5 Small Wavefunction (small r)       ; LINE(pp/wf-5s0.5,  836, 38); 0.00000000E+00; 1e-12
match; 5s0.5 Small Wavefunction (large r)       ; LINE(pp/wf-5s0.5, 1117, 38); 0.00000000E+00; 1e-12
match; 5s0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 56); 2.91162924E-01; 2e-06
match; 5s0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 56); -1.32012596E-05; 3e-10
match; 5s0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 74); 0.00000000E+00; 1e-12
match; 5s0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 74); 0.00000000E+00; 1e-12

match; 5p0.5 Large Wavefunction (small r)       ; LINE(pp/wf-5p0.5,  836, 20); 1.60535901E-01; 9e-07
match; 5p0.5 Large Wavefunction (large r)       ; LINE(pp/wf-5p0.5, 1117, 20); 1.87501937E-04; 4e-09
match; 5p0.5 Small Wavefunction (small r)       ; LINE(pp/wf-5p0.5,  836, 38); 0.00000000E+00; 1e-12
match; 5p0.5 Small Wavefunction (large r)       ; LINE(pp/wf-5p0.5, 1117, 38); 0.00000000E+00; 1e-12
match; 5p0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5p0.5,  836, 56); 3.50544749E-01; 1e-06
match; 5p0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5p0.5, 1117, 56); -1.74450064E-04; 3e-09
match; 5p0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5p0.5,  836, 74); 0.00000000E+00; 1e-12
match; 5p0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5p0.5, 1117, 74); 0.00000000E+00; 1e-12

match; 5p1.5 Large Wavefunction (small r)       ; LINE(pp/wf-5p1.5,  836, 20); 1.45536067E-01; 10e-07
match; 5p1.5 Large Wavefunction (large r)       ; LINE(pp/wf-5p1.5, 1117, 20); 3.04049781E-04; 5e-09
match; 5p1.5 Small Wavefunction (small r)       ; LINE(pp/wf-5p1.5,  836, 38); 0.00000000E+00; 1e-12
match; 5p1.5 Small Wavefunction (large r)       ; LINE(pp/wf-5p1.5, 1117, 38); 0.00000000E+00; 1e-12
match; 5p1.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5p1.5,  836, 56); 3.11570694E-01; 2e-06
match; 5p1.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5p1.5, 1117, 56); -2.65313074E-04; 5e-09
match; 5p1.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5p1.5,  836, 74); 0.00000000E+00; 1e-12
match; 5p1.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5p1.5, 1117, 74); 0.00000000E+00; 1e-12

#Pseudopotentials
match; s0.5 Pseudopotential (small r); LINE(pp/pp-s0.5,  836, 20); -9.90881118E-01; 5e-05
match; s0.5 Pseudopotential (large r); LINE(pp/pp-s0.5, 1117, 20); -7.98767348E-01; 10e-10
match; p0.5 Pseudopotential (small r); LINE(pp/pp-p0.5,  836, 20); -3.26876453E+00; 4e-05
match; p0.5 Pseudopotential (large r); LINE(pp/pp-p0.5, 1117, 20); -7.98767348E-01; 10e-10
match; p1.5 Pseudopotential (small r); LINE(pp/pp-p1.5,  836, 20); -3.42346645E+00; 3e-05
match; p1.5 Pseudopotential (large r); LINE(pp/pp-p1.5, 1117, 20); -7.98767348E-01; 10e-10


Input: 03-rel.04-rtm.inp
#TM coefficients
match; 5s0.5 c0  Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 4); -1.646469739E+00; 10e-06
match; 5s0.5 c2  Coefficient; GREP(pp/info, 'State: 5s0.5', 37, 4); 1.301798345E+00; 2e-05
match; 5s0.5 c4  Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 5); -3.389357861E-01; 1e-05
match; 5s0.5 c6  Coefficient; GREP(pp/info, 'State: 5s0.5', 37, 5); -1.209398232E-01; 2e-06
match; 5s0.5 c8  Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 6); 7.489728266E-02; 5e-07
match; 5s0.5 c10 Coefficient; GREP(pp/info, 'State: 5s0.5', 37, 6); -1.402881338E-02; 2e-07
match; 5s0.5 c12 Coefficient; GREP(pp/info, 'State: 5s0.5', 12, 7); 9.352596840E-04; 2e-08
match; 5p0.5 c0  Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 4); -1.187646141E+00; 8e-06
match; 5p0.5 c2  Coefficient; GREP(pp/info, 'State: 5p0.5', 37, 4); 2.050286895E-01; 9e-06
match; 5p0.5 c4  Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 5); -6.005251930E-03; 5e-07
match; 5p0.5 c6  Coefficient; GREP(pp/info, 'State: 5p0.5', 37, 5); -9.156984038E-02; 3e-06
match; 5p0.5 c8  Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 6); 3.321980705E-02; 1e-06
match; 5p0.5 c10 Coefficient; GREP(pp/info, 'State: 5p0.5', 37, 6); -4.810301466E-03; 2e-07
match; 5p0.5 c12 Coefficient; GREP(pp/info, 'State: 5p0.5', 12, 7); 2.601891156E-04; 1e-08
match; 5p1.5 c0  Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 4); -1.274147296E+00; 9e-06
match; 5p1.5 c2  Coefficient; GREP(pp/info, 'State: 5p1.5', 37, 4); 1.565267348E-01; 8e-06
match; 5p1.5 c4  Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 5); -3.500088397E-03; 4e-07
match; 5p1.5 c6  Coefficient; GREP(pp/info, 'State: 5p1.5', 37, 5); -6.322340050E-02; 2e-06
match; 5p1.5 c8  Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 6); 2.037617924E-02; 5e-07
match; 5p1.5 c10 Coefficient; GREP(pp/info, 'State: 5p1.5', 37, 6); -2.612946321E-03; 7e-08
match; 5p1.5 c12 Coefficient; GREP(pp/info, 'State: 5p1.5', 12, 7); 1.247862538E-04; 4e-09

#Pseudo-Wavefunctions
match; 5s0.5 Large Wavefunction (small r)       ; LINE(pp/wf-5s0.5,  836, 20); 2.61090043E-01; 2e-06
match; 5s0.5 Large Wavefunction (large r)       ; LINE(pp/wf-5s0.5, 1117, 20); 1.01127542E-05; 2e-10
match; 5s0.5 Small Wavefunction (small r)       ; LINE(pp/wf-5s0.5,  836, 38); 1.06251809E-03; 7e-09
match; 5s0.5 Small Wavefunction (large r)       ; LINE(pp/wf-5s0.5, 1117, 38); -4.81680531E-08; 1e-12
match; 5s0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 56); 2.91178875E-01; 2e-06
match; 5s0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 56); -1.32012596E-05; 3e-10
match; 5s0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 74); 2.53438451E-03; 3e-08
match; 5s0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 74); 6.31692724E-08; 1e-12

match; 5p0.5 Large Wavefunction (small r)       ; LINE(pp/wf-5p0.5,  836, 20); 1.60509034E-01; 9e-07
match; 5p0.5 Large Wavefunction (large r)       ; LINE(pp/wf-5p0.5, 1117, 20); 1.87501937E-04; 4e-09
match; 5p0.5 Small Wavefunction (small r)       ; LINE(pp/wf-5p0.5,  836, 38); 3.61768307E-03; 2e-08
match; 5p0.5 Small Wavefunction (large r)       ; LINE(pp/wf-5p0.5, 1117, 38); -4.99900368E-07; 8e-12
match; 5p0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5p0.5,  836, 56); 3.50509070E-01; 9e-07
match; 5p0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5p0.5, 1117, 56); -1.74450064E-04; 3e-09
match; 5p0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5p0.5,  836, 74); 1.03985552E-03; 5e-08
match; 5p0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5p0.5, 1117, 74); 4.57758749E-07; 7e-12

match; 5p1.5 Large Wavefunction (small r)       ; LINE(pp/wf-5p1.5,  836, 20); 1.45509240E-01; 10e-07
match; 5p1.5 Large Wavefunction (large r)       ; LINE(pp/wf-5p1.5, 1117, 20); 3.04049781E-04; 5e-09
match; 5p1.5 Small Wavefunction (small r)       ; LINE(pp/wf-5p1.5,  836, 38); 7.66188919E-05; 4e-09
match; 5p1.5 Small Wavefunction (large r)       ; LINE(pp/wf-5p1.5, 1117, 38); -1.07881678E-06; 2e-11
match; 5p1.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5p1.5,  836, 56); 3.11535355E-01; 2e-06
match; 5p1.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5p1.5, 1117, 56); -2.65313074E-04; 5e-09
match; 5p1.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5p1.5,  836, 74); 2.70372865E-04; 2e-08
match; 5p1.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5p1.5, 1117, 74); 9.62616210E-07; 2e-11

#Pseudopotentials
match; s0.5 Pseudopotential (small r); LINE(pp/pp-s0.5,  836, 20); -9.89766259E-01; 5e-05
match; s0.5 Pseudopotential (large r); LINE(pp/pp-s0.5, 1117, 20); -7.98767327E-01; 1e-12
match; p0.5 Pseudopotential (small r); LINE(pp/pp-p0.5,  836, 20); -3.26808515E+00; 4e-05
match; p0.5 Pseudopotential (large r); LINE(pp/pp-p0.5, 1117, 20); -7.98767327E-01; 1e-12
match; p1.5 Pseudopotential (small r); LINE(pp/pp-p1.5,  836, 20); -3.42267290E+00; 3e-05
match; p1.5 Pseudopotential (large r); LINE(pp/pp-p1.5, 1117, 20); -7.98767327E-01; 1e-12


Input: 03-rel.05-mrpp.inp

#MRPP coefficients
match; 4s0.5 c0  Coefficient; GREP(pp/info, 'State: 4s0.5', 12, 4); -1.838843937E-01; 10e-04
match; 4s0.5 c2  Coefficient; GREP(pp/info, 'State: 4s0.5', 37, 4); 6.315526149E+00; 9e-03
match; 4s0.5 c4  Coefficient; GREP(pp/info, 'State: 4s0.5', 12, 5); -7.977174106E+00; 3e-02
match; 4s0.5 c6  Coefficient; GREP(pp/info, 'State: 4s0.5', 37, 5); -1.891710957E+01; 2e-02
match; 4s0.5 c8  Coefficient; GREP(pp/info, 'State: 4s0.5', 12, 6); 5.726017175E+01; 2e-02
match; 4s0.5 c10 Coefficient; GREP(pp/info, 'State: 4s0.5', 37, 6); -6.435025568E+01; 5e-02
match; 4s0.5 c12 Coefficient; GREP(pp/info, 'State: 4s0.5', 12, 7); 3.767193143E+01; 4e-02
match; 4s0.5 c14 Coefficient; GREP(pp/info, 'State: 4s0.5', 37, 7); -1.143624931E+01; 2e-02
match; 4s0.5 c16 Coefficient; GREP(pp/info, 'State: 4s0.5', 12, 8); 1.426947846E+00; 2e-03
match; 4p0.5 c0  Coefficient; GREP(pp/info, 'State: 4p0.5', 12, 4); 1.473328089E+00; 3e-03
match; 4p0.5 c2  Coefficient; GREP(pp/info, 'State: 4p0.5', 37, 4); 3.961491690E-01; 2e-02
match; 4p0.5 c4  Coefficient; GREP(pp/info, 'State: 4p0.5', 12, 5); -2.241915638E-02; 2e-03
match; 4p0.5 c6  Coefficient; GREP(pp/info, 'State: 4p0.5', 37, 5); -1.578525903E+01; 2e-01
match; 4p0.5 c8  Coefficient; GREP(pp/info, 'State: 4p0.5', 12, 6); 3.328941820E+01; 3e-01
match; 4p0.5 c10 Coefficient; GREP(pp/info, 'State: 4p0.5', 37, 6); -3.227075788E+01; 3e-01
match; 4p0.5 c12 Coefficient; GREP(pp/info, 'State: 4p0.5', 12, 7); 1.692352355E+01; 2e-01
match; 4p0.5 c14 Coefficient; GREP(pp/info, 'State: 4p0.5', 37, 7); -4.661434022E+00; 5e-02
match; 4p0.5 c16 Coefficient; GREP(pp/info, 'State: 4p0.5', 12, 8); 5.307595262E-01; 5e-03
match; 4p1.5 c0  Coefficient; GREP(pp/info, 'State: 4p1.5', 12, 4); 1.384998249E+00; 2e+00
match; 4p1.5 c2  Coefficient; GREP(pp/info, 'State: 4p1.5', 37, 4); 3.508185521E-01; 4e-01
match; 4p1.5 c4  Coefficient; GREP(pp/info, 'State: 4p1.5', 12, 5); -1.758195169E-02; 2e-02
match; 4p1.5 c6  Coefficient; GREP(pp/info, 'State: 4p1.5', 37, 5); -1.251580755E+01; 2e+01
match; 4p1.5 c8  Coefficient; GREP(pp/info, 'State: 4p1.5', 12, 6); 2.442864175E+01; 3e+01
match; 4p1.5 c10 Coefficient; GREP(pp/info, 'State: 4p1.5', 37, 6); -2.190729470E+01; 3e+01
match; 4p1.5 c12 Coefficient; GREP(pp/info, 'State: 4p1.5', 12, 7); 1.062556283E+01; 2e+01
match; 4p1.5 c14 Coefficient; GREP(pp/info, 'State: 4p1.5', 37, 7); -2.706473158E+00; 3e+00
match; 4p1.5 c16 Coefficient; GREP(pp/info, 'State: 4p1.5', 12, 8); 2.849510908E-01; 3e-01

#Pseudo-Wavefunctions
match; 4s0.5 Large Wavefunction (small r)       ; LINE(pp/wf-4s0.5,  836, 20); 2.16251397E+00; 3e+00
match; 4s0.5 Large Wavefunction (large r)       ; LINE(pp/wf-4s0.5, 1117, 20); 0.00000000E+00; 1e-12
match; 4s0.5 Small Wavefunction (small r)       ; LINE(pp/wf-4s0.5,  836, 38); 0.00000000E+00; 1e-12
match; 4s0.5 Small Wavefunction (large r)       ; LINE(pp/wf-4s0.5, 1117, 38); 0.00000000E+00; 1e-12
match; 4s0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-4s0.5,  836, 56); 2.79266015E+00; 3e+00
match; 4s0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-4s0.5, 1117, 56); 0.00000000E+00; 1e-12
match; 4s0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-4s0.5,  836, 74); 0.00000000E+00; 1e-12
match; 4s0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-4s0.5, 1117, 74); 0.00000000E+00; 1e-12
match; 5s0.5 Large Wavefunction (small r)       ; LINE(pp/wf-5s0.5,  836, 20); -8.21856190E-01; 2e+00
match; 5s0.5 Large Wavefunction (large r)       ; LINE(pp/wf-5s0.5, 1117, 20); 1.01127542E-05; 2e-10
match; 5s0.5 Small Wavefunction (small r)       ; LINE(pp/wf-5s0.5,  836, 38); 0.00000000E+00; 2e-03
match; 5s0.5 Small Wavefunction (large r)       ; LINE(pp/wf-5s0.5, 1117, 38); 0.00000000E+00; 5e-08
match; 5s0.5 Large Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 56); 2.20041907E-01; 8e-02
match; 5s0.5 Large Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 56); -1.32012596E-05; 3e-10
match; 5s0.5 Small Wavefunction Deriv. (small r); LINE(pp/wf-5s0.5,  836, 74); 0.00000000E+00; 3e-03
match; 5s0.5 Small Wavefunction Deriv. (large r); LINE(pp/wf-5s0.5, 1117, 74); 0.00000000E+00; 7e-08

#Pseudopotentials
match; s0.5 Pseudopotential (small r); LINE(pp/pp-s0.5,  836, 20); -3.73616006E+01; 4e+01
match; s0.5 Pseudopotential (large r); LINE(pp/pp-s0.5, 1117, 20); -2.59599383E+00; 2e+00
match; p0.5 Pseudopotential (small r); LINE(pp/pp-p0.5,  836, 20); -3.97453058E+01; 4e+01
match; p0.5 Pseudopotential (large r); LINE(pp/pp-p0.5, 1117, 20); -2.59599383E+00; 2e+00
match; p1.5 Pseudopotential (small r); LINE(pp/pp-p1.5,  836, 20); -3.81186556E+01; 4e+01
match; p1.5 Pseudopotential (large r); LINE(pp/pp-p1.5, 1117, 20); -2.59599383E+00; 2e+00
