!! Copyright (C) 2004-2010 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module splines_m
  use global_m
  use messages_m
  use gsl_interface_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
     module procedure copy_splines
  end interface

  interface operator (==)
     module procedure equal_splines
  end interface


                    !---Derived Data Types---!

  !> This derived data type is the only one acessible to other modules.
  !> It is just a pointer to the splines information
  type spline_t
    private
    type(spline_info_t), pointer :: info
  end type spline_t

  type spline_info_t
    integer(POINTER_SIZE) :: acc    !< accelerator gsl object
    integer(POINTER_SIZE) :: spl    !< spline gsl object
    integer               :: n_ptrs !< how many instances of spline_t are pointing to this instance of spline_info_t
  end type spline_info_t


                    !---Public/Private Statements---!

  private
  public :: spline_t, &
            spline_null, &
            spline_init, &
            assignment(=), &
            operator(==), &
            spline_end, &
            spline_eval, &
            spline_eval_deriv, &
            spline_eval_deriv2, &
            spline_eval_integ, &
            spline_mesh_transfer


contains

  !-----------------------------------------------------------------------
  !> Nullifies the components of the spline.                              
  !-----------------------------------------------------------------------
  subroutine spline_null(spline)
    type(spline_t), intent(out) :: spline

    nullify(spline%info)

  end subroutine spline_null

  !-----------------------------------------------------------------------
  !> Given a set of data points (x_i,y_i) it computes an interpolation     
  !> function of a chosen type. The available interpolation types are:     
  !>                                                                       
  !>  * linear (interp_type = 1)                                            
  !>  * polynomial (interp_type = 2)
  !>  * cubic spline with natural boundary conditions  (interp_type = 3)
  !>  * cubic spline with periodic boundary conditions (interp_type = 4)
  !>  * Akima spline with natural boundary conditions  (interp_type = 5)  
  !>  * Akima spline with periodic boundary conditions (interp_type = 6)  
  !-----------------------------------------------------------------------
  subroutine spline_init(spline, n, x, y, interp_type)
    type(spline_t), intent(inout) :: spline      !< pointer to the spline information
    integer,        intent(in)    :: interp_type !< interpolation type
    integer,        intent(in)    :: n           !< number of data-points
    real(R8),       intent(in)    :: x(n)        !< x coordinates of the data points
    real(R8),       intent(in)    :: y(n)        !< y coordinates of the data points

    real(R8), allocatable :: auxx(:), auxy(:)

    ASSERT(.not.associated(spline%info))

    !Allocate memory
    allocate(spline%info)

    !Store all the information about the interpolation function
    spline%info%n_ptrs = 1
    spline%info%acc = 0
    spline%info%spl = 0
    call gsl_interp_accel_alloc(spline%info%acc)
    call gsl_spline_alloc(spline%info%spl, n, interp_type)
    allocate(auxx(n), auxy(n))
    auxy = y !The use of an auxiliary array prevents some types of errors
    auxx = x
    call gsl_spline_init(spline%info%spl, n, auxx(1), auxy(1))
    deallocate(auxx, auxy)

  end subroutine spline_init

  !-----------------------------------------------------------------------
  !> Makes spline_a equal to spline_b. That is done by making both splines
  !> component info point to the same interpolation information.          
  !-----------------------------------------------------------------------
  subroutine copy_splines(spline_a, spline_b)
    type(spline_t), intent(inout) :: spline_a
    type(spline_t), intent(in)    :: spline_b

    ASSERT(associated(spline_b%info))

    !Deallocate the memory previously assigned to spline_a
    call spline_end(spline_a)

    !Make spline_a point to the same interpolation info than spline_b
    spline_a%info => spline_b%info

    !Now we have an addicional pointer to the spline information
    spline_a%info%n_ptrs = spline_a%info%n_ptrs + 1

  end subroutine copy_splines

  !-----------------------------------------------------------------------
  !> Returns true if spline_a and spline_b interpolation objects are equal.
  !> Returns false if they are not.                                        
  !-----------------------------------------------------------------------
  function equal_splines(spline_a, spline_b)
    type(spline_t), intent(in) :: spline_a, spline_b
    logical :: equal_splines

    equal_splines = associated(spline_a%info, spline_b%info)

  end function equal_splines

  !-----------------------------------------------------------------------
  !> Frees the interpolation object spline.                               
  !-----------------------------------------------------------------------
  subroutine spline_end(spline)
    type(spline_t), intent(inout) :: spline

    if (associated(spline%info)) then
      if (spline%info%n_ptrs > 1) then
        !There are more pointers pointing to this spline information
        spline%info%n_ptrs = spline%info%n_ptrs - 1
        nullify(spline%info)
      else
        !This is the only pointer pointing to this spline information
        call gsl_spline_free(spline%info%spl)
        call gsl_interp_accel_free(spline%info%acc)
        deallocate(spline%info)
      end if
    end if

  end subroutine spline_end

  !-----------------------------------------------------------------------
  !> Returns the interpolated value for a given point.                    
  !-----------------------------------------------------------------------
  function spline_eval(spline, x)
    type(spline_t), intent(in) :: spline !< interpolation object
    real(R8),       intent(in) :: x      !< point where to evaluate the function
    real(R8) :: spline_eval

    spline_eval = gsl_spline_eval(x, spline%info%spl, spline%info%acc)
 
  end function spline_eval

  !-----------------------------------------------------------------------
  !> Returns the derivative of an interpolated function for a given point.
  !-----------------------------------------------------------------------
  function spline_eval_deriv(spline, x)
    type(spline_t), intent(in) :: spline !< interpolation object
    real(R8),       intent(in) :: x      !< point where to evaluate the derivative
    real(R8) :: spline_eval_deriv
    
    spline_eval_deriv = gsl_spline_eval_deriv(x, spline%info%spl, spline%info%acc)
 
  end function spline_eval_deriv

  !-----------------------------------------------------------------------
  !> Returns the second derivative of an interpolated function for a given
  !> point.                                                               
  !-----------------------------------------------------------------------
  function spline_eval_deriv2(spline, x)
    type(spline_t), intent(in) :: spline !< interpolation object
    real(R8),       intent(in) :: x      !< point where to evaluate the second derivativ
    real(R8) :: spline_eval_deriv2
    
    spline_eval_deriv2 = gsl_spline_eval_deriv2(x, spline%info%spl, spline%info%acc)
 
  end function spline_eval_deriv2

  !-----------------------------------------------------------------------
  !> Returns the numerical integral of an interpolated function over the  
  !> range [a,b].                                                         
  !-----------------------------------------------------------------------
  function spline_eval_integ(spline, a, b)
    type(spline_t), intent(in) :: spline !< interpolation object
    real(R8),       intent(in) :: a      !< lower integration bound
    real(R8),       intent(in) :: b      !< upper integration bound
    real(R8) :: spline_eval_integ

    spline_eval_integ = gsl_spline_eval_integ(a, b, spline%info%spl, spline%info%acc)
 
  end function spline_eval_integ

  !-----------------------------------------------------------------------
  !> Having a function on a given mesh A, this routines returns the values
  !> of that function on a mesh B, by interpolating the function.         
  !-----------------------------------------------------------------------
  subroutine spline_mesh_transfer(np_a, x_a, y_a, np_b, x_b, y_b, interp_type)
    integer,  intent(in)  :: np_a        !< mesh A number of points
    integer,  intent(in)  :: np_b        !< mesh B number of points
    integer,  intent(in)  :: interp_type !< interpolation type
    real(R8), intent(in)  :: x_a(np_a)   !< mesh A points
    real(R8), intent(in)  :: y_a(np_a)   !< values of the function on mesh A
    real(R8), intent(in)  :: x_b(np_b)   !< mesh B points
    real(R8), intent(out) :: y_b(np_b)   !< values of the function on mesh B

    integer :: i
    type(spline_t) :: spl

    call push_sub("spline_mesh_transfer")

    call spline_null(spl)
    call spline_init(spl, np_a, x_a, y_a, interp_type)
    do i = 1, np_b
      y_b(i) = spline_eval(spl, x_b(i))
    end do
    call spline_end(spl)

    call pop_sub()
  end subroutine spline_mesh_transfer

end module splines_m
