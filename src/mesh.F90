!! Copyright (C) 2004-2012 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module mesh_m
  use global_m
  use oct_parser_m
  use messages_m
  use utilities_m
  use math_m
  use splines_m
  use finite_diff_m
  use units_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
     module procedure mesh_copy
  end interface

  interface operator (==)
     module procedure equal_mesh
  end interface


                    !---Derived Data Types---!

  type mesh_t
    integer,  private          :: type !< mesh type
    real(R8), public           :: a    !< mesh parameter
    real(R8), public           :: b    !< mesh parameter
    integer,  public           :: np   !< mesh number of points
    real(R8), public,  pointer :: r(:) !< mesh points

    integer, private :: intrp_method !< Method to interpolate functions between meshes
    integer, private :: integ_method !< Method used to calculate integrals
    integer, private :: deriv_method !< Method used to calculate derivatives
    integer, private :: interp_range
    integer, private :: fd_order
    type(fd_operator_t), private :: deriv
    type(fd_operator_t), private :: deriv2
    type(fd_operator_t), private :: deriv3
  end type mesh_t


                    !---Global Variables---!

  !Mesh types
  integer, parameter :: MESH_LINEAR  = 1, &
                        MESH_LOG1    = 2, &
                        MESH_LOG2    = 3

  !Interpolation, derivatives, and integrals methods
  integer, parameter :: MESH_CUBIC_SPLINE = 1, &
                        MESH_FINITE_DIFF  = 2


                    !---Public/Private Statements---!

  private
  public :: mesh_t, &
            mesh_null, &
            mesh_init, &
            mesh_init_from_input, &
            mesh_generation, &
            mesh_save, &
            mesh_load, &
            mesh_transfer, &
            mesh_truncate, &
            mesh_extrapolate, &
            mesh_derivative, &
            mesh_derivative2, &
            mesh_derivative3, &
            mesh_integrate, &
            mesh_primitive, &
            mesh_gradient, &
            mesh_divergence, &
            mesh_laplacian, &
            mesh_output_params, &
            mesh_end, &
            assignment(=), &
            operator(==), &
            MESH_LOG1, &
            MESH_LINEAR, &
            MESH_LOG2, &
            MESH_CUBIC_SPLINE, &
            MESH_FINITE_DIFF

contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of mesh m.             
  !-----------------------------------------------------------------------
  subroutine mesh_null(m)
    type(mesh_t), intent(out) :: m

    call push_sub("mesh_null")

    m%type = 0
    m%a    = M_ZERO
    m%b    = M_ZERO
    m%np   = 0
    m%intrp_method = 0
    m%interp_range = 0
    m%deriv_method = 0
    m%integ_method = 0
    m%fd_order = 0
    call fd_operator_null(m%deriv)
    call fd_operator_null(m%deriv2)
    call fd_operator_null(m%deriv3)
    nullify(m%r)

    call pop_sub()
  end subroutine mesh_null

  !-----------------------------------------------------------------------
  !> Initializes the calculation mesh by reading the mesh parameter from  
  !> the input file. The default parameters depend on the nuclear charge. 
  !-----------------------------------------------------------------------
  subroutine mesh_init(m, type, deriv_method, r1, rmax, np, a, fd_order)
    type(mesh_t),           intent(inout) :: m            !< mesh object
    integer,                intent(in)    :: type
    integer,                intent(in)    :: deriv_method
    real(R8),               intent(in)    :: r1
    real(R8),               intent(in)    :: rmax
    integer,      optional, intent(in)    :: np
    integer,      optional, intent(in)    :: fd_order
    real(R8),     optional, intent(in)    :: a

    call push_sub("mesh_init")

    ASSERT(m%type == 0)
    ASSERT(present(a) .or. present(np))
    ASSERT(deriv_method /= MESH_FINITE_DIFF .or. (deriv_method == MESH_FINITE_DIFF .and. present(fd_order)))

    !Generate mesh
    if (present(a)) then
      call mesh_generation(m, type, r1, rmax, a=a)
    else
      call mesh_generation(m, type, r1, rmax, n=np)
    end if

    m%deriv_method = deriv_method
    if (m%deriv_method == MESH_FINITE_DIFF) then
      m%fd_order = fd_order
      call fd_operator_init(m%deriv,  1, m%fd_order, m%np, m%r)
      call fd_operator_init(m%deriv2, 2, m%fd_order, m%np, m%r)
      call fd_operator_init(m%deriv3, 3, m%fd_order, m%np, m%r)
    end if
    m%integ_method = MESH_CUBIC_SPLINE
    m%intrp_method = MESH_CUBIC_SPLINE
    m%interp_range = 25

    call pop_sub()
  end subroutine mesh_init

  !-----------------------------------------------------------------------
  !> Initializes the calculation mesh by reading the mesh parameter from  
  !> the input file. The default parameters depend on the nuclear charge. 
  !-----------------------------------------------------------------------
  subroutine mesh_init_from_input(z, m)
    real(R8),     intent(in)    :: z !< uclear charge
    type(mesh_t), intent(inout) :: m !< mesh object

    logical  :: is_def_np, is_def_a
    integer  :: type, np, deriv_method, fd_order
    real(R8) :: r1, rmax, a

    call push_sub("mesh_init_from_input")

    ASSERT(m%type == 0)

    !Read input options
    type = oct_parse_f90_int('MeshType', MESH_LOG1)
    select case (type)
    case (MESH_LOG1, MESH_LOG2, MESH_LINEAR)
    case default
      message(1) =  "Illegal MeshType."
      call write_fatal(1)
    end select

    r1 = oct_parse_f90_double('MeshStartingPoint', sqrt(z)*1.0E-5)
    if (r1 <= M_ZERO) then
      message(1) =  "MeshStartingPoint can not take negative values."
      call write_fatal(1)
    end if
    r1 = r1*units_in%length%factor

    rmax = oct_parse_f90_double('MeshOutmostPoint', sqrt(z)*M_THIRTY)
    rmax = rmax*units_in%length%factor
    if (rmax <= M_ZERO .or. rmax < r1) then
      message(1) = "MeshOutmostPoint can''t take negative values"
      message(2) = "and must be greater than the mesh start point."
      call write_fatal(2)
    end if
    
    is_def_np = oct_parse_f90_isdef('MeshNumberOfPoints')
    is_def_a  = oct_parse_f90_isdef('MeshParameter')
    if (is_def_np .and. is_def_a) then
      message(1) = "MeshNumberOfPoints and MeshParameter input options"
      message(2) = "can not be used at the same time."
      call write_fatal(2)
    end if

    fd_order = 0
    deriv_method = oct_parse_f90_int('MeshDerivMethod', MESH_CUBIC_SPLINE)
    select case (deriv_method)
    case (MESH_CUBIC_SPLINE)
    case (MESH_FINITE_DIFF)
      fd_order = oct_parse_f90_int('MeshFiniteDiffOrder', 4)
      if (fd_order < 1) then
        message(1) = "MeshFiniteDerivOrder must be greater than 0."
        call write_fatal(1)
      end if
    case default
      message(1) =  "Illegal MeshDerivMethod."
      call write_fatal(1)
    end select

    if (is_def_a) then
      a = oct_parse_f90_double('MeshParameter', M_DIME)
      if (a < M_ZERO) then
        message(1) = "MeshParameter can not take negative values."
        call write_fatal(1)
      end if

      call mesh_init(m, type, deriv_method, r1, rmax, a=a, fd_order=fd_order)
    else
      np = oct_parse_f90_int('MeshNumberOfPoints', int(sqrt(z))*200)
      if (np <= 3) then
        message(1) = "Mesh must have at least 3 points."
        call write_fatal(1)
      end if

      call mesh_init(m, type, deriv_method, r1, rmax, np=np, fd_order=fd_order)
    end if

    call pop_sub()
  end subroutine mesh_init_from_input

  !-----------------------------------------------------------------------
  !> Initializes a mesh and generates the mesh points. Note that only one 
  !> of the optional variables n and a should be used.                    
  !-----------------------------------------------------------------------
  subroutine mesh_generation(m, type, r1, rn, n, a)
    type(mesh_t),           intent(inout) :: m    !< mesh
    integer,                intent(in)    :: type !< mesh type
    real(R8),               intent(in)    :: r1   !< starting point
    real(R8),               intent(in)    :: rn   !< ending point
    integer,      optional, intent(in)    :: n    !< number of points
    real(R8),     optional, intent(in)    :: a    !< mesh parameter a

    integer :: i
    real(R8) :: a1, a2, n1, n2, am, nm, f1, fm

    call push_sub("mesh_generation")

    !Check optional arguments
    if (present(n) .and. present(a)) then
      message(1) = "Only one optional argument can be set in mesh_generation"
      call write_fatal(1)
    end if

    !Set the mesh type, the number of points, the first point and the parameter a
    m%type = type
    if (present(n)) then
      m%np = n
      allocate(m%r(m%np))
      m%r(1) = r1
      select case (m%type)
      case (MESH_LINEAR)
        m%a = (rn - r1)/real(n-1, R8)
      case (MESH_LOG1)
        m%a = log(rn/r1)/real(n - 1,R8)
      case (MESH_LOG2)
        a1 = 1.0e-8_r8
        f1 = func(r1, rn, real(n,R8), a1)
        a2 = M_ONE
        do
          am = (a2 + a1)*M_HALF
          fm = func(r1, rn, real(n,R8), am)
          if (M_HALF*abs(a1 - a2) < 1.0e-16) exit
          if (fm*f1 > M_ZERO) then
            a1 = am; f1 = fm
          else
            a2 = am
          end if
        end do
        m%a = am
      end select

    elseif (present(a)) then
      select case (m%type)
      case (MESH_LINEAR)
        m%np = int((rn - r1)/a)
      case (MESH_LOG1)
        m%np = int(log(rn/r1)/a) + 1
      case (MESH_LOG2)
        n1 = M_ONE
        f1 = func(r1, rn, n1, a)
        n2 = 10000.0_r8
        do
          nm = (n2 + n1)*M_HALF
          fm = func(r1, rn, nm, a)
          if (M_HALF*abs(n1 - n2) < 1.0e-12) exit
          if (fm*f1 > M_ZERO) then
            n1 = nm; f1 = fm
          else
            n2 = nm
          end if
        end do
        m%np = int(nm)
      end select
      allocate(m%r(m%np))
      m%r(1) = r1
      m%a = a

    end if

    !Set the parameter b and the remaining points
    select case (m%type)
    case (MESH_LINEAR)
      m%b = M_ZERO
      do i = 2, m%np-1
        m%r(i) = m%r(i-1) + m%a
      end do
    case (MESH_LOG1)
      m%b = r1/exp(m%a)
      do i = 2, m%np-1
        m%r(i) = exp(m%a)*m%r(i-1)
      end do
    case (MESH_LOG2)
      m%b = r1/(exp(m%a) - M_ONE)
      do i = 2, m%np-1
        m%r(i) = m%r(i-1)*exp(m%a) + r1
      end do
    end select
    m%r(m%np) = rn

    call pop_sub()
  contains

    real(R8) function func(r1, rn, n, a)
      real(R8), intent(in) :: r1, rn, a, n
      func = exp(n*a)*r1 - M_ONE*r1 - rn*exp(a) + rn*M_ONE
    end function func

  end subroutine mesh_generation

  !-----------------------------------------------------------------------
  !> Copies m_a mesh to m_b.                                              
  !-----------------------------------------------------------------------
  subroutine mesh_copy(m_a, m_b)
    type(mesh_t), intent(inout) :: m_a
    type(mesh_t), intent(in)    :: m_b

    call push_sub("mesh_copy")

    call mesh_end(m_a)
    m_a%type = m_b%type
    m_a%a    = m_b%a
    m_a%b    = m_b%b
    m_a%np   = m_b%np
    m_a%intrp_method = m_b%intrp_method
    m_a%interp_range = m_b%interp_range
    m_a%deriv_method = m_b%deriv_method
    m_a%integ_method = m_b%integ_method
    allocate(m_a%r(m_a%np))
    m_a%r = m_b%r
    if (m_a%deriv_method == MESH_FINITE_DIFF) then
      m_a%fd_order = m_b%fd_order
      m_a%deriv = m_b%deriv
      m_a%deriv2 = m_b%deriv2
      m_a%deriv3 = m_b%deriv3
    end if

    call pop_sub()
  end subroutine mesh_copy

  !-----------------------------------------------------------------------
  !> Returns true if m_a and m_b are the same mesh; false otherwise.      
  !-----------------------------------------------------------------------
  function equal_mesh(m_a, m_b)
    type(mesh_t), intent(in) :: m_a, m_b
    logical :: equal_mesh

    call push_sub("equal_mesh")

    equal_mesh = (m_a%type == m_b%type .and. m_a%a == m_b%a .and. &
                  m_a%b == m_b%b .and. m_a%np == m_b%np)

    call pop_sub()
  end function equal_mesh

  !-----------------------------------------------------------------------
  !> Writes the mesh information to a file.                               
  !-----------------------------------------------------------------------
  subroutine mesh_save(unit, m)
    integer,      intent(in) :: unit !< file unit number
    type(mesh_t), intent(in) :: m    !< mesh to be written

    integer :: i

    call push_sub("mesh_save")

    ASSERT(m%type /= 0)

    write(unit) m%type, m%a, m%b, m%np, m%intrp_method, m%interp_range, m%deriv_method, &
                m%integ_method, m%fd_order
    write(unit) (m%r(i), i=1, m%np)

    call pop_sub()
  end subroutine mesh_save

  !-----------------------------------------------------------------------
  !> Reads the mesh information from a file.                              
  !-----------------------------------------------------------------------
  subroutine mesh_load(unit, m)
    integer,      intent(in)    :: unit !< file unit number
    type(mesh_t), intent(inout) :: m    !< mesh to be read

    integer :: i

    call push_sub("mesh_load")

    ASSERT(m%type == 0)

    read(unit) m%type, m%a, m%b, m%np, m%intrp_method, m%interp_range, m%deriv_method, &
                m%integ_method, m%fd_order
    allocate(m%r(m%np))
    read(unit) (m%r(i), i=1, m%np)

    if (m%deriv_method == MESH_FINITE_DIFF) then
      call fd_operator_init(m%deriv, 1,  m%fd_order, m%np, m%r)
      call fd_operator_init(m%deriv2, 2, m%fd_order, m%np, m%r)
      call fd_operator_init(m%deriv3, 3, m%fd_order, m%np, m%r)
    end if

    call pop_sub()
  end subroutine mesh_load

  !-----------------------------------------------------------------------
  !> Truncates a mesh so that the last point is rt                        
  !-----------------------------------------------------------------------
  subroutine mesh_truncate(m, rt)
    type(mesh_t), intent(inout) :: m
    real(R8),     intent(in)    :: rt

    real(R8), allocatable :: r(:)

    call push_sub("mesh_truncate")

    ASSERT(m%type /= 0)

    m%np = locate(m%r, rt, 0)
    allocate(r(m%np+1))
    r(1:m%np) = m%r(1:m%np)
    if (m%r(m%np) /= rt) then
      r(m%np + 1) = rt
      m%np = m%np + 1
    end if
    deallocate(m%r)
    allocate(m%r(m%np))
    m%r(1:m%np) = r(1:m%np)
    deallocate(r)

    if (m%deriv_method == MESH_FINITE_DIFF) then
      call fd_operator_end(m%deriv)
      call fd_operator_end(m%deriv2)
      call fd_operator_init(m%deriv, 1,  m%fd_order, m%np, m%r)
      call fd_operator_init(m%deriv2, 2, m%fd_order, m%np, m%r)
      call fd_operator_init(m%deriv3, 3, m%fd_order, m%np, m%r)
    end if

    call pop_sub()
  end subroutine mesh_truncate

  !-----------------------------------------------------------------------
  !> Having a function on a mesh A, this routines returns the values of   
  !> that function on a mesh A, by interpolating the function.            
  !-----------------------------------------------------------------------
  subroutine mesh_transfer(m_a, fa, m_b, fb)
    type(mesh_t), intent(in)  :: m_a        !< mesh A
    type(mesh_t), intent(in)  :: m_b        !< mesh B
    real(R8),     intent(in)  :: fa(m_a%np) !< values of the function on mesh A
    real(R8),     intent(out) :: fb(m_b%np) !< values of the function on mesh B

    call push_sub("mesh_transfer")

    ASSERT(m_a%type /= 0 .and. m_b%type /= 0)

    select case (m_a%intrp_method)
    case (MESH_CUBIC_SPLINE)
      call spline_mesh_transfer(m_a%np, m_a%r, fa, m_b%np, m_b%r, fb, 3)

    case default
      write(message(1),'("Illegal interpolation method in mesh_transfer: ",I1)') &
           m_a%intrp_method
      call write_fatal(1)
    end select

    call pop_sub()
  end subroutine mesh_transfer

  function mesh_extrapolate(m, f, r)
    !-----------------------------------------------------------------------
    !> Returns the value of f at r                                          
    !-----------------------------------------------------------------------
    type(mesh_t), intent(in)  :: m       !< mesh
    real(R8),     intent(in)  :: f(m%np) !< values of the function on mesh
    real(R8),     intent(in)  :: r       !< point where to evaluate the function
    real(R8) :: mesh_extrapolate

    integer :: i, ii, if
    type(spline_t) :: spl

    call push_sub("mesh_extrapolate")

    select case (m%intrp_method)
    case (MESH_CUBIC_SPLINE)
      i = locate(m%r, r, 0)
      ii = max(1, i-m%interp_range)
      if = min(m%np, i+m%interp_range)
      call spline_null(spl)
      call spline_init(spl, if-ii+1, m%r(ii:if), f(ii:if), 3)
      mesh_extrapolate = spline_eval(spl, r)
      call spline_end(spl)

    case default
      write(message(1),'("Illegal interpolation method in mesh_eval: ",I1)') &
           m%intrp_method
      call write_fatal(1)
    end select

    call pop_sub()
  end function mesh_extrapolate

  !-----------------------------------------------------------------------
  !> Returns the derivate of f                                            
  !-----------------------------------------------------------------------
  function mesh_derivative(m, f)
    type(mesh_t), intent(in)  :: m       !< mesh
    real(R8),     intent(in)  :: f(m%np) !< values of the function on mesh
    real(R8) :: mesh_derivative(m%np)

    integer :: i
    type(spline_t) :: spl

    call push_sub("mesh_derivative")

    select case (m%deriv_method)
    case (MESH_CUBIC_SPLINE)
      call spline_null(spl)
      call spline_init(spl, m%np, m%r, f, 3)
      do i = 1, m%np
        mesh_derivative(i) = spline_eval_deriv(spl, m%r(i))
      end do
      call spline_end(spl)

    case (MESH_FINITE_DIFF)
      call fd_operator_apply(m%deriv, f, mesh_derivative)

    case default
      write(message(1),'("Illegal numerical derivative method in mesh_derivative: ",I1)') &
           m%deriv_method
      call write_fatal(1)
    end select

    call pop_sub()
  end function mesh_derivative

  !-----------------------------------------------------------------------
  !> Returns the second derivate of f                                     
  !-----------------------------------------------------------------------
  function mesh_derivative2(m, f)
    type(mesh_t), intent(in)  :: m       !< mesh
    real(R8),     intent(in)  :: f(m%np) !< values of the function on mesh
    real(R8) :: mesh_derivative2(m%np)

    integer :: i
    type(spline_t) :: spl

    call push_sub("mesh_derivative2")

    select case (m%deriv_method)
    case (MESH_CUBIC_SPLINE)
      call spline_null(spl)
      call spline_init(spl, m%np, m%r, f, 3)
      do i = 1, m%np
        mesh_derivative2(i) = spline_eval_deriv2(spl, m%r(i))
      end do
      call spline_end(spl)

    case (MESH_FINITE_DIFF)
      call fd_operator_apply(m%deriv2, f, mesh_derivative2)

    case default
      write(message(1),'("Illegal numerical derivative method in mesh_derivative2: ",I1)') &
           m%deriv_method
      call write_fatal(1)
    end select

    call pop_sub()
  end function mesh_derivative2

  !-----------------------------------------------------------------------
  !> Returns the third derivate of f                                      
  !-----------------------------------------------------------------------
  function mesh_derivative3(m, f)
    type(mesh_t), intent(in)  :: m       !< mesh
    real(R8),     intent(in)  :: f(m%np) !< values of the function on mesh
    real(R8) :: mesh_derivative3(m%np)

    integer :: i
    type(spline_t) :: spl

    call push_sub("mesh_derivative3")

    select case (m%deriv_method)
    case (MESH_CUBIC_SPLINE)
      call spline_null(spl)
      call spline_init(spl, m%np, m%r, mesh_derivative(m, f), 3)
      do i = 1, m%np
        mesh_derivative3(i) = spline_eval_deriv2(spl, m%r(i))
      end do
      call spline_end(spl)

    case (MESH_FINITE_DIFF)
      call fd_operator_apply(m%deriv3, f, mesh_derivative3)

    case default
      write(message(1),'("Illegal numerical derivative method in mesh_derivative3: ",I1)') &
           m%deriv_method
      call write_fatal(1)
    end select

    call pop_sub()
  end function mesh_derivative3

  !-----------------------------------------------------------------------
  !> Returns the integral of f between a and b. Default values for a and b
  !> are 0 and the last point of the mesh.                                
  !-----------------------------------------------------------------------
  function mesh_integrate(m, f, a, b, dv)
    type(mesh_t),           intent(in) :: m        !< mesh
    real(R8),               intent(in) :: f(m%np)  !< values of the function on mesh
    real(R8),     optional, intent(in) :: a        !< lower bound of the integration interval. Default is the origin.
    real(R8),     optional, intent(in) :: b        !< upper bound of the integration interval. Default is the outmost point of the mesh.
    real(R8),     optional, intent(in) :: dv(m%np) !< volume element for integration. Default is r**2
    real(R8) :: mesh_integrate

    real(R8) :: a_, b_, dv_(m%np)
    type(spline_t) :: spl

    call push_sub("mesh_integrate")

    if (present(a)) then
      a_ = a
    else
      a_ = M_ZERO
    end if

    if (present(b)) then
      b_ = b
    else
      b_ = m%r(m%np)
    end if

    if (present(dv)) then
      dv_ = dv
    else
      dv_ = m%r**2
    end if
    
    select case (m%integ_method)
    case (MESH_CUBIC_SPLINE)      
      call spline_null(spl)
      call spline_init(spl, m%np, m%r, f*dv_, 3)
      if (a_ < m%r(1)) then
        mesh_integrate = polynomial_extrapolative_integration(4, m%r(1:4), f(1:4)*dv_(1:4), a_, m%r(1))
        a_ = m%r(1)
      else
        mesh_integrate = M_ZERO
      end if
      mesh_integrate = mesh_integrate + spline_eval_integ(spl, a_, b_)
      call spline_end(spl)

    case default
      write(message(1),'("Illegal numerical integration method in mesh_integrate: ",I1)') &
           m%integ_method
      call write_fatal(1)
    end select

    call pop_sub()
  end function mesh_integrate

  !-----------------------------------------------------------------------
  !> Returns the primitive of f.                                          
  !-----------------------------------------------------------------------
  function mesh_primitive(m, f, dv)
    type(mesh_t),           intent(in) :: m        !< mesh
    real(R8),               intent(in) :: f(m%np)  !< values of the function on mesh
    real(R8),     optional, intent(in) :: dv(m%np) !< volume element for integration. Default is r**2
    real(R8) :: mesh_primitive(m%np)

    integer :: i
    real(R8) :: dv_(m%np)
    type(spline_t) :: spl

    call push_sub("mesh_primitive")

    if (present(dv)) then
      dv_ = dv
    else
      dv_ = m%r**2
    end if

    select case (m%integ_method)
    case (MESH_CUBIC_SPLINE)
      call spline_null(spl)
      call spline_init(spl, m%np, m%r, f*dv_, 3)  
      mesh_primitive(1) = polynomial_extrapolative_integration(4, m%r(1:4), f(1:4)*dv_(1:4), M_ZERO, m%r(1))    
      do i = 2, m%np
        mesh_primitive(i) = mesh_primitive(i-1) + spline_eval_integ(spl, m%r(i-1), m%r(i))
      end do
      call spline_end(spl)

    case default
      write(message(1),'("Illegal numerical integration method in mesh_primitive: ",I1)') &
           m%integ_method
      call write_fatal(1)
    end select

    call pop_sub()
  end function mesh_primitive

  !-----------------------------------------------------------------------
  !> Returns the gradient of f                                            
  !-----------------------------------------------------------------------
  function mesh_gradient(m, f)
    type(mesh_t), intent(in)  :: m       !< mesh
    real(R8),     intent(in)  :: f(m%np) !< values of the function on mesh
    real(R8) :: mesh_gradient(m%np)

    call push_sub("mesh_gradient")

    mesh_gradient = mesh_derivative(m, f)

    call pop_sub()
  end function mesh_gradient

  !-----------------------------------------------------------------------
  !> Returns the divergence of f                                          
  !-----------------------------------------------------------------------
  function mesh_divergence(m, f)
    type(mesh_t), intent(in)  :: m       !< mesh
    real(R8),     intent(in)  :: f(m%np) !< values of the function on mesh
    real(R8) :: mesh_divergence(m%np)

    call push_sub("mesh_divergence")

    mesh_divergence = M_TWO/m%r*f + mesh_derivative(m, f)

    call pop_sub()
  end function mesh_divergence

  !-----------------------------------------------------------------------
  !> Returns the laplacian of f                                           
  !-----------------------------------------------------------------------
  function mesh_laplacian(m, f)
    type(mesh_t), intent(in)  :: m       !< mesh
    real(R8),     intent(in)  :: f(m%np) !< values of the function on mesh
    real(R8) :: mesh_laplacian(m%np)

    call push_sub("mesh_laplacian")

    mesh_laplacian = M_TWO/m%r*mesh_derivative(m, f) + mesh_derivative2(m, f)

    call pop_sub()
  end function mesh_laplacian

  !-----------------------------------------------------------------------
  !> Writes the mesh input options and the mesh paremeters in a nice      
  !> readable format. If a unit number is provided, it writes it to a file
  !> otherwise it writes it to the standard output.                       
  !-----------------------------------------------------------------------
  subroutine mesh_output_params(m, unit, verbose_limit)
    type(mesh_t),           intent(in) :: m
    integer,      optional, intent(in) :: unit
    integer,      optional, intent(in) :: verbose_limit

    call push_sub("mesh_output_params")

    ASSERT(m%type /= 0)

    message(1) = ""
    message(2) = "Mesh information:"
    select case (m%type)
    case (MESH_LINEAR)
      message(3) = "  Type: linear"
    case (MESH_LOG1)
      message(3) = "  Type: logarithmic [ri = b*exp(a*i)]"
    case (MESH_LOG2)
      message(3) = "  Type: logarithmic [ri = b*(exp(a*i) - 1)]"
    end select
    write(message(4), '("  Mesh starting point:   ",ES8.2E2,1X,A)') m%r(1)/units_out%length%factor, trim(units_out%length%abbrev)
    write(message(5), '("  Mesh outmost point:    ",F7.3,1X,A)') m%r(m%np)/units_out%length%factor, trim(units_out%length%abbrev)
    write(message(6), '("  Mesh parameters (a, b): ",ES12.5E2,", ",ES12.5E2)') m%a, m%b
    write(message(7), '("  Mesh number of points:  ",I5)') m%np
    select case (m%deriv_method)
    case (MESH_CUBIC_SPLINE)
      message(8) = "  Derivatives Method: Cubic splines"
    case (MESH_FINITE_DIFF)
      write(message(8), '("  Derivatives Method: Finite differences (order = ",I2,")")') m%fd_order
    end select
    call write_info(8, verbose_limit=verbose_limit, unit=unit)
 
    call pop_sub()
  end subroutine mesh_output_params

  !-----------------------------------------------------------------------
  !> Frees all memory associated to the mesh object m.                    
  !-----------------------------------------------------------------------
  subroutine mesh_end(m)
    type(mesh_t), intent(inout) :: m

    call push_sub("mesh_end")

    m%type = 0
    m%a    = M_ZERO
    m%b    = M_ZERO
    m%np   = 0
    m%intrp_method = 0
    m%interp_range = 0
    m%deriv_method = 0
    m%integ_method = 0
    if (associated(m%r)) then
      deallocate(m%r)
    end if
    m%fd_order = 0
    call fd_operator_end(m%deriv)
    call fd_operator_end(m%deriv2)
    call fd_operator_end(m%deriv3)

    call pop_sub()
  end subroutine mesh_end

end module mesh_m
