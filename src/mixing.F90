!! Copyright (C) 2004-2012 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! This module was originaly written by A. Castro and M. Oliveira 
!! for the Octopus program.
!!

module mixing_m
  use global_m
  use oct_parser_m
  use messages_m
  use linalg_m
  implicit none


                    !---Derived Data Types---!

  type mixing_t
    private
    integer  :: scheme
    real(R8) :: alpha          !< vnew = (1-alpha)*vin + alpha*vout
    integer  :: nsteps         !< number of steps used to extrapolate the new vector
    integer  :: last_ipos
    integer  :: np
    real(R8), pointer :: df(:,:)
    real(R8), pointer :: dv(:,:)
    real(R8), pointer :: f_old(:)
    real(R8), pointer :: vin_old(:)
  end type mixing_t


                    !---Global Variables---!

  integer, parameter :: MIX_LINEAR  = 1, &
                        MIX_BROYDEN = 2


                   !---Public/Private Statements---!

  private
  public :: mixing_t, &
            mixing_null, &
            mixing_init, &
            mixing_end, &
            mixing, &
            MIX_LINEAR, &
            MIX_BROYDEN

contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of mix.                
  !-----------------------------------------------------------------------
  subroutine mixing_null(mix)
    type(mixing_t), intent(out) :: mix

    call push_sub("mixing_null")

    mix%scheme = 0
    mix%alpha  = M_ZERO
    mix%nsteps = 0
    mix%last_ipos = 0
    mix%np = 0
    nullify(mix%df)
    nullify(mix%dv)
    nullify(mix%f_old)
    nullify(mix%vin_old)

    call pop_sub()
  end subroutine mixing_null

  !-----------------------------------------------------------------------
  !> Reads mixing parameters from the input file. If using the Broyden    
  !> mixing, it also allocates arrays to store information from previous  
  !> iterations needed for the extrapolation.                             
  !-----------------------------------------------------------------------
  subroutine mixing_init(mix, np)
    type(mixing_t), intent(inout) :: mix !< mixing object
    integer,        intent(in)    :: np  !< dimension of the vectors

    call push_sub("mixing_init")

    !Read and check input parameters
    mix%scheme = oct_parse_f90_int("MixingScheme", MIX_BROYDEN)
    if(mix%scheme < 1 .or. mix%scheme > 2) then
      message(1) = 'Illegal MixingScheme.'
      call write_fatal(1)
    end if

    mix%alpha = oct_parse_f90_double("Mixing", 0.3_r8)
    if(mix%alpha <= M_ZERO .or. mix%alpha > M_ONE) then
      write(message(1), '(a, f14.6,a)') "Input: '", mix%alpha, "' is not a valid Mixing"
      message(2) = '(0 < Mixing <= 1)'
      call write_fatal(2)
    end if

    if (mix%scheme == MIX_BROYDEN) then
      mix%nsteps = oct_parse_f90_int("MixingSteps", 3)
      if(mix%nsteps <= 1) then
        write(message(1),'("Input: ",I4," is not a valid MixingSteps")') mix%nsteps
        message(2) = '(1 < MixingSteps)'
        call write_fatal(2)
      end if
    end if

    if (mix%scheme == MIX_BROYDEN) then
      !Allocate memory
      allocate(mix%df(np, mix%nsteps), mix%dv(np, mix%nsteps), &
               mix%vin_old(np), mix%f_old(np))
      mix%df      = M_ZERO
      mix%vin_old = M_ZERO
      mix%dv      = M_ZERO
      mix%f_old   = M_ZERO
    end if
    mix%last_ipos = 0
    mix%np = np

    !Output input info
    select case (mix%scheme)
    case (MIX_LINEAR)
      message(1) = "  Mixing scheme: Linear Mixing"
    case (MIX_BROYDEN)
      message(1) = "  Mixing scheme: Modified Broyden's Method"
    end select
    write(message(2),'("  Mixing: ",F5.3)') mix%alpha
    call write_info(2,20)

    call pop_sub()
  end subroutine mixing_init

  !-----------------------------------------------------------------------
  !> Deallocates all the memory associated with the mixing object.        
  !-----------------------------------------------------------------------
  subroutine mixing_end(mix)
    type(mixing_t), intent(inout) :: mix

    call push_sub("mixing_end")

    if (associated(mix%df))      deallocate(mix%df)
    if (associated(mix%dv))      deallocate(mix%dv)
    if (associated(mix%vin_old)) deallocate(mix%vin_old)
    if (associated(mix%f_old))   deallocate(mix%f_old)

    call pop_sub()
  end subroutine mixing_end

  !-----------------------------------------------------------------------
  !> Driver routine that calls the appropriate extrapolation routine.     
  !-----------------------------------------------------------------------
  subroutine mixing(mix, iter, vin, vout, vnew)
    type(mixing_t), intent(inout) :: mix          !< mixing object
    integer,        intent(in)    :: iter         !< iteration number
    real(R8),       intent(in)    :: vin(mix%np)  !< input vector from the current iteration
    real(R8),       intent(in)    :: vout(mix%np) !< output vector from the current iteration
    real(R8),       intent(out)   :: vnew(mix%np) !< extrapolated vector to be used as the input vector for the next iteration

    call push_sub("mixing")

    !Check the iteration number
    if (iter < 1) then
      message(1) = 'Wrong number of iterations in suboutine mixing.'
      call write_fatal(1)
    end if

    !Call the appropriate extrapolation routine
    select case (mix%scheme)
    case (MIX_LINEAR)
      call mixing_linear(mix%alpha, mix%np, vin, vout, vnew)
    case (MIX_BROYDEN)
      call mixing_broyden(mix, vin, vout, vnew, iter)
    end select

    call pop_sub()
  end subroutine mixing

  !-----------------------------------------------------------------------
  !> Performs the linear mixing.                                          
  !-----------------------------------------------------------------------
  subroutine mixing_linear(alpha, np, vin, vout, vnew)
    real(r8), intent(in)  :: alpha    !< mixing parameter (vnew = alpha*vout + (1-alpha)*vin)
    integer,  intent(in)  :: np       !< dimension of the vectors
    real(R8), intent(in)  :: vin(np)  !< input vector from the current iteration
    real(R8), intent(in)  :: vout(np) !< output vector from the current iteration
    real(R8), intent(out) :: vnew(np) !< extrapolated vector to be used as the input vector for the next iteration

    call push_sub("mixing_linear")

    vnew = alpha*vout + (M_ONE - alpha)*vin

    call pop_sub()
  end subroutine mixing_linear

  !-----------------------------------------------------------------------
  !> Modified Broyden second method.                                      
  !> This routines stores the information needed for future extrapolations
  !> and calls the extrapolation routine.                                 
  !-----------------------------------------------------------------------
  subroutine mixing_broyden(mix, vin, vout, vnew, iter)
    type(mixing_t), intent(inout) :: mix          !< mixing object
    integer,        intent(in)    :: iter         !< iteration number
    real(R8),       intent(in)    :: vin(mix%np)  !< input vector from the current iteration
    real(R8),       intent(in)    :: vout(mix%np) !< output vector from the current iteration
    real(R8),       intent(out)   :: vnew(mix%np) !< extrapolated vector to be used as the input vector for the next iteration

    integer  :: ipos, iter_used
    real(r8) :: gamma
    real(R8), allocatable :: f(:)

    call push_sub("mixing_broyden")

    allocate(f(mix%np))
    f = vout - vin
    if(iter > 1) then
      !Store df and dv from current iteration
      ipos = mod(mix%last_ipos, mix%nsteps) + 1

      mix%df(:, ipos) = f - mix%f_old
      mix%dv(:, ipos) = vin - mix%vin_old

      gamma = sqrt(dot_product(mix%df(:, ipos), mix%df(:, ipos)))
      if(gamma > 1e-8_r8) then
        gamma = M_ONE/gamma
      else
        gamma = M_ONE
      endif
      mix%df(:, ipos) = mix%df(:, ipos)*gamma
      mix%dv(:, ipos) = mix%dv(:, ipos)*gamma

      mix%last_ipos = ipos
    end if

    !Store residual and vin for next iteration
    mix%vin_old = vin
    mix%f_old   = f

    !Extrapotate new vector
    iter_used = min(iter - 1, mix%nsteps)
    call broyden_extrapolation(mix%alpha, mix%np, vin, vout, vnew, iter_used, f,   &
                               mix%df(:, 1:iter_used), mix%dv(:, 1:iter_used))

    deallocate(f)

    call pop_sub()
  end subroutine mixing_broyden

  !-----------------------------------------------------------------------
  !> Performs the Broyden extrapolation.                                  
  !-----------------------------------------------------------------------
  subroutine broyden_extrapolation(alpha, np, vin, vout, vnew, iter_used, f, df, dv)
    real(r8), intent(in)  :: alpha             !< mixing parameter for the linear terms
    integer,  intent(in)  :: np                !< dimension of the vectors
    integer,  intent(in)  :: iter_used         !< 
    real(r8), intent(in)  :: vin(np)           !< input vector from the current iteration
    real(r8), intent(in)  :: vout(np)          !< output vector from the current iteration
    real(r8), intent(in)  :: f(np)             !< 
    real(r8), intent(in)  :: df(np, iter_used) !< 
    real(r8), intent(in)  :: dv(np, iter_used) !< 
    real(r8), intent(out) :: vnew(np)          !< extrapolated vector to be used as the input vector for the next iteration

    real(r8), parameter :: w0 = M_CENT

    integer  :: i, j
    real(r8) :: beta(iter_used, iter_used), gamma, work(iter_used), w(iter_used)

    call push_sub("broyden_extrapolation")

    if (iter_used <= 2) then
      !Linear mixing
      vnew = alpha*vout + (M_ONE - alpha)*vin
      return
    end if

    w = M_FIVE

    !Compute matrix beta
    beta = M_ZERO
    do i = 1, iter_used
      do j = i + 1, iter_used
        beta(i, j) = w(i)*w(j)*dot_product(df(:, j), df(:, i))
        beta(j, i) = beta(i, j)
      end do
      beta(i, i) = w0**2 + w(i)**2
    end do

    !Invert matrix beta
    call matrix_invert(iter_used, beta)

    do i = 1, iter_used
      work(i) = dot_product(df(:, i), f(:))
    end do

    !Linear mixing term
    vnew = vin + alpha*f

    !Other terms
    do i = 1, iter_used
      gamma = M_ZERO
      do j = 1, iter_used
        gamma = gamma + beta(j, i)*w(j)*work(j)
      end do
      vnew = vnew - w(i)*gamma*(alpha*df(:, i) + dv(:, i))
    end do

    call pop_sub()
  end subroutine broyden_extrapolation

end module mixing_m
