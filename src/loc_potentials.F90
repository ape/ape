!! Copyright (C) 2004-2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module loc_potentials_m
  use global_m
  use messages_m
  use io_m
  use units_m
  use splines_m
  use mesh_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
     module procedure loc_potential_copy
  end interface


                    !---Derived Data Types---!

  type loc_potential_t
    private
    integer :: type !< All-electron (AE) or parametrized (PARAM)

    real(R8) :: z   !< The nuclear part z/r of an all-electron potential

    !The parametrized potential
    real(R8), pointer :: v(:)
    type(spline_t)    :: v_spl
    type(spline_t)    :: vp_spl
    type(spline_t)    :: vpp_spl
  end type loc_potential_t


                    !---Global Variables---!

  integer, parameter :: AE    = 1, &
                        PARAM = 2


                    !---Public/Private Statements---!

  private
  public :: loc_potential_t, &
            loc_potential_null, &
            loc_potential_init, &
            assignment(=), &
            loc_potential_end, &
            loc_potential_save, &
            loc_potential_load, &
            loc_v, &
            loc_dvdr, &
            loc_d2vdr2, &
            loc_integral, &
            loc_potential_nuclear_charge, &
            loc_potential_is_ae, &
            loc_potential_debug, &
            loc_potential_output


contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of the potential.      
  !-----------------------------------------------------------------------
  subroutine loc_potential_null(potential)
    type(loc_potential_t), intent(out) :: potential

    call push_sub("loc_potential_null")

    potential%type = 0

    potential%z = M_ZERO
    nullify(potential%v)
    call spline_null(potential%v_spl)
    call spline_null(potential%vp_spl)
    call spline_null(potential%vpp_spl)

    call pop_sub()
  end subroutine loc_potential_null

  !-----------------------------------------------------------------------
  !> Initialize the potential.                                            
  !-----------------------------------------------------------------------
  subroutine loc_potential_init(potential, m, z, v)
    type(loc_potential_t),           intent(inout) :: potential !< potential to be initialized
    type(mesh_t),                    intent(in)    :: m         !< mesh
    real(R8),              optional, intent(in)    :: z         !< nuclear charge
    real(R8),              optional, intent(in)    :: v(m%np)   !< values of the potential on the mesh

    call push_sub("loc_potential_init")

    ASSERT(potential%type == 0)
    ASSERT(present(z) .or. present(v))

    if (present(z)) then
      potential%type = AE
      potential%z = z
    end if

    if (present(v)) then
      potential%type = PARAM
      allocate(potential%v(m%np))
      potential%v = v
      call spline_init(potential%v_spl, m%np, m%r, v, 3)
      call spline_init(potential%vp_spl, m%np, m%r, mesh_derivative(m, v), 3)
      call spline_init(potential%vpp_spl, m%np, m%r, mesh_derivative2(m, v), 3)
    end if

    call pop_sub()
  end subroutine loc_potential_init

  !-----------------------------------------------------------------------
  !> Copies the potential potential_a to potential potential_b.           
  !-----------------------------------------------------------------------
  subroutine loc_potential_copy(potential_a, potential_b)
    type(loc_potential_t), intent(inout) :: potential_a
    type(loc_potential_t), intent(in)    :: potential_b

    call push_sub("loc_potential_copy")

    ASSERT(potential_b%type /= 0)

    call loc_potential_end(potential_a)

    potential_a%type = potential_b%type

    select case (potential_b%type)
    case (AE)
      potential_a%z = potential_b%z
    case (PARAM)
      allocate(potential_a%v(size(potential_b%v)))
      potential_a%v = potential_b%v
      potential_a%v_spl= potential_b%v_spl
      potential_a%vp_spl= potential_b%vp_spl
      potential_a%vpp_spl= potential_b%vpp_spl
    end select

    call pop_sub()
  end subroutine loc_potential_copy

  !-----------------------------------------------------------------------
  !> Frees all the memory associated to the potential.                    
  !-----------------------------------------------------------------------
  subroutine loc_potential_end(potential)
    type(loc_potential_t), intent(inout) :: potential

    call push_sub("loc_potential_end")

    potential%type = 0

    potential%z = M_ZERO
    if (associated(potential%v)) then
      deallocate (potential%v)
    end if
    call spline_end(potential%v_spl)
    call spline_end(potential%vp_spl)
    call spline_end(potential%vpp_spl)

    call pop_sub()
  end subroutine loc_potential_end

  !-----------------------------------------------------------------------
  !> Writes the potential to a file.                                      
  !-----------------------------------------------------------------------
  subroutine loc_potential_save(unit, m, potential)
    integer,               intent(in) :: unit      !< file unit number
    type(mesh_t),          intent(in) :: m         !< the mesh
    type(loc_potential_t), intent(in) :: potential !< potential to be written

    integer :: i

    call push_sub("loc_potential_save")

    ASSERT(potential%type /= 0)

    write(unit) potential%type

    select case (potential%type)
    case (AE)
      write(unit) potential%z
    case (PARAM)
      do i = 1, m%np
        write(unit) potential%v(i)
      end do
    end select

    call pop_sub()
  end subroutine loc_potential_save

  !-----------------------------------------------------------------------
  !> Reads the potential from a file.                                     
  !-----------------------------------------------------------------------
  subroutine loc_potential_load(unit, m, potential)
    integer,               intent(in)    :: unit      !< file unit number
    type(mesh_t),          intent(in)    :: m         !< the mesh
    type(loc_potential_t), intent(inout) :: potential !< potential to be read

    integer :: i

    call push_sub("loc_potential_load")

    ASSERT(potential%type == 0)

    read(unit) potential%type

    select case (potential%type)
    case (AE)
      read(unit) potential%z
    case (PARAM)
      allocate(potential%v(m%np))
      do i = 1, m%np
        read(unit) potential%v(i)
      end do
      call spline_init(potential%v_spl, m%np, m%r, potential%v, 3)
      call spline_init(potential%vp_spl, m%np, m%r, mesh_derivative(m, potential%v), 3)
      call spline_init(potential%vpp_spl, m%np, m%r, mesh_derivative2(m, potential%v), 3)
    end select

    call pop_sub()
  end subroutine loc_potential_load

  !-----------------------------------------------------------------------
  !> Returns the value of the loc potential felt by an electron at        
  !> radius r.                                                            
  !-----------------------------------------------------------------------
  function loc_v(potential, r)
    type(loc_potential_t), intent(in) :: potential
    real(R8),              intent(in) :: r
    real(R8) :: loc_v

    ASSERT(potential%type /= 0)

    select case (potential%type)
    case (AE)
      loc_v = -potential%z/r
    case (PARAM)
      loc_v = spline_eval(potential%v_spl, r)
    end select

  end function loc_v

  !-----------------------------------------------------------------------
  !> Returns the value of the first derivative of the loc potential felt  
  !> by an electron at radius r.                                          
  !-----------------------------------------------------------------------
  function loc_dvdr(potential, r)
    type(loc_potential_t), intent(in) :: potential
    real(R8),              intent(in) :: r
    real(R8) :: loc_dvdr

    ASSERT(potential%type /= 0)

    select case (potential%type)
    case (AE)
      loc_dvdr = potential%z/r**2
    case (PARAM)
      loc_dvdr = spline_eval(potential%vp_spl, r)
    end select

  end function loc_dvdr

  !-----------------------------------------------------------------------
  !> Returns the value of the second derivative of the loc potential      
  !> felt by an electron at radius r.                                     
  !-----------------------------------------------------------------------
  function loc_d2vdr2(potential, r)
    type(loc_potential_t), intent(in) :: potential
    real(R8),              intent(in) :: r
    real(R8) :: loc_d2vdr2

    ASSERT(potential%type /= 0)

    select case (potential%type)
    case (AE)
      loc_d2vdr2 = -M_TWO*potential%z/r**3
    case (PARAM)
      loc_d2vdr2 = spline_eval(potential%vpp_spl, r)
    end select

  end function loc_d2vdr2

  !-----------------------------------------------------------------------
  !> Returns the value of the integral of the potential between ra and rb.
  !-----------------------------------------------------------------------
  function loc_integral(potential, ra, rb)
    type(loc_potential_t), intent(in) :: potential
    real(R8),              intent(in) :: ra, rb
    real(R8) :: loc_integral

    ASSERT(potential%type /= 0)
    ASSERT(potential%type /= AE)
 
    loc_integral = spline_eval_integ(potential%v_spl, ra, rb)

  end function loc_integral

  !-----------------------------------------------------------------------
  !> Returns the nuclear charge.                                          
  !-----------------------------------------------------------------------
  function loc_potential_nuclear_charge(potential)
    type(loc_potential_t), intent(in) :: potential
    real(R8) :: loc_potential_nuclear_charge

    loc_potential_nuclear_charge = potential%z

  end function loc_potential_nuclear_charge

  !-----------------------------------------------------------------------
  !> Returns true if the loc potential is an all-electron potential.      
  !-----------------------------------------------------------------------
  function loc_potential_is_ae(potential)
    type(loc_potential_t), intent(in) :: potential
    logical :: loc_potential_is_ae

    loc_potential_is_ae = potential%type == AE

  end function loc_potential_is_ae

  !-----------------------------------------------------------------------
  ! Prints debug information to a file.                                   
  !-----------------------------------------------------------------------
  subroutine loc_potential_debug(unit, m, potential)
    integer,               intent(in) :: unit
    type(mesh_t),          intent(in) :: m
    type(loc_potential_t), intent(in) :: potential

    integer :: i

    call push_sub("loc_potential_debug")

    ASSERT(potential%type /= 0)
	
    select case (potential%type)
    case (AE)
      write(unit,'("# Loc potential type: All-electron")')
      write(unit,'("# Nuclear Charge: ",F10.4)') potential%z 
    case (PARAM)
      write(unit,'("# Loc potential type: Parametrized")')
      do i = 1, m%np
        write(unit,'(ES10.3E2,1X,ES10.3E2)') m%r(i), potential%v(i)
      end do
    end select

    call pop_sub()
  end subroutine loc_potential_debug

  !-----------------------------------------------------------------------
  !> Writes the potential to a file in a format suitable for plotting.    
  !-----------------------------------------------------------------------
  subroutine loc_potential_output(potential, m, filename)
    type(loc_potential_t), intent(in) :: potential
    type(mesh_t),          intent(in) :: m
    character(len=*),      intent(in) :: filename

    integer  :: i, unit
    real(R8) :: ue, ul

    call push_sub("loc_potential_output")

    ASSERT(potential%type /= 0)

    call io_open(unit, file=trim(filename))

    ul = units_out%length%factor
    ue = units_out%energy%factor

    write(unit,'("# ")')
    write(unit,'("# Energy units: ",A)') trim(units_out%energy%name)
    write(unit,'("# Length units: ",A)') trim(units_out%length%name)
    write(unit,'("#")')

    write(unit,'("# ",34("-"))')
    write(unit,'("# |",7X,"r",7X,"|",6X,"v(r)",6X,"|")')
    write(unit,'("# ",34("-"))')
    do i = 1, m%np
      write(unit,'(3X,ES14.8E2,3X,ES15.8E2)') m%r(i)/ul, loc_v(potential, m%r(i))/ue
    end do

    close(unit)

    call pop_sub()
  end subroutine loc_potential_output

end module loc_potentials_m
