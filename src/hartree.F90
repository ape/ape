!! Copyright (C) 2004-2012 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

module hartree_m
  use global_m
  use io_m
  use units_m
  use mesh_m
  implicit none


                    !---Public/Private Statements---!

  private
  public :: hartree_potential, &
            hartree_screening_function, &
            hartree_output

contains

  !-----------------------------------------------------------------------
  !> Calculates the Hartree potencial for a given density.                
  !-----------------------------------------------------------------------
  subroutine hartree_potential(m, density, charge, vh, eh)
    type(mesh_t),           intent(in)  :: m             !< the radial mesh
    real(R8),               intent(in)  :: density(m%np) !< the charge density
    real(R8),               intent(in)  :: charge        !< the charge
    real(R8),     optional, intent(out) :: vh(:,:)       !< the Hartree potential
    real(R8),     optional, intent(out) :: eh            !< the Hartree energy

    integer :: i
    real(R8), allocatable :: y0(:), int1(:), int2(:)
    
    !Allocate memory
    allocate(y0(m%np), int1(m%np), int2(m%np))

    !Calculate Hartree screening function
    call hartree_screening_function(m, density*m%r**2*M_FOUR*M_PI, 0, y0)
    
    !Force potential to have the correct asymptotic limit
    if (y0(m%np) /= M_ZERO .and. charge /= M_ZERO) then
      y0 = y0/y0(m%np)*charge
    end if

    if (present(vh)) then
      do i = 1, m%np
        vh(i,:) = y0(i)/m%r(i)
      end do
    end if

    if (present(eh)) then
      eh = M_TWO*M_PI*mesh_integrate(m, y0*density, dv=m%r)
    end if

    !Deallocate arrays
    deallocate(y0, int1, int2)

  end subroutine hartree_potential

  !-----------------------------------------------------------------------
  !> Calculates the Hartree screening function for a given density defined
  !> as:
  !>
  !>  \f$ Y^k(\rho,r_1) = r_1 \int \d r_2 \frac{r_<^k}{r_>^k}rho(r_2)\f$
  !>
  !> Note that we follow the usual convention and do not include the
  !> \f$r_2^2\f$ factor in the volume element.
  !-----------------------------------------------------------------------
  subroutine hartree_screening_function(m, density, k, yk)
    type(mesh_t), intent(in)  :: m             !< radial mesh
    real(R8),     intent(in)  :: density(m%np) !< charge density
    integer,      intent(in)  :: k             !< order of the function
    real(R8),     intent(out) :: yk(:)         !< screening function

    integer :: i
    real(R8), allocatable :: int1(:), int2(:)
    
    !Allocate memory
    allocate(int1(m%np), int2(m%np))

    !Calculate the primitives
    int1 = mesh_primitive(m, density, dv=m%r**k)
    int2 = mesh_primitive(m, density, dv=m%r**(-k-1))

    !Calculate the function
    do i = 1, m%np
      yk(i) = int1(i)/m%r(i)**k + (int2(m%np) - int2(i))*m%r(i)**(k+1)
    end do

    !Deallocate arrays
    deallocate(int1, int2)

  end subroutine hartree_screening_function

  !-----------------------------------------------------------------------
  !> Calculates the Hartree potencial for a given density.                
  !-----------------------------------------------------------------------
  subroutine hartree_output(m, density, charge, dir)
    type(mesh_t),     intent(in) :: m             !< the radial mesh
    real(R8),         intent(in) :: density(m%np) !< the charge density
    real(R8),         intent(in) :: charge        !< the charge
    character(len=*), intent(in) :: dir           !< directory where to write the output

    integer :: i, unit
    real(R8), allocatable :: vh(:,:)

    allocate(vh(m%np,1))

    !Get hartree potential
    call hartree_potential(m, density, charge, vh=vh)

    !Open file
    call io_open(unit, file=trim(dir)//"/v_h")

    !Write header
    write(unit,'("# ")')
    write(unit,'("# Energy units: ",A)') trim(units_out%energy%name)
    write(unit,'("# Length units: ",A)') trim(units_out%length%name)
    write(unit,'("#")')
    write(unit,'("# ",36("-"))')
    write(unit,'("# |",8X,"r",7X,"|",7X,"v(r)",6X,"|")')
    write(unit,'("# ",36("-"))')

    !Ouput
    do i = 1, m%np
      write(unit,'(4(3X,ES15.8E2))') m%r(i)/units_out%length%factor, &
           vh(i,1)/units_out%energy%factor
    end do

    close(unit)
    deallocate(vh)

  end subroutine hartree_output

end module hartree_m
