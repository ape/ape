!! Copyright (C) 2004-2013 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module parser_symbols_m
  use global_m
  use oct_parser_m
  use mesh_m
  use mixing_m
  use quantum_numbers_m
  use eigensolver_m
  use xc_f03_lib_m
  use functionals_m
  use xc_m
  use ode_integrator_m
  use wave_equations_m
  use states_m
  use states_batch_m
  use ps_io_m
  use atom_m
  use numerical_tests_m
  use run_ape_m
  implicit none


                   !---Interfaces---!

#ifdef HAVE_LIBXC5
  interface
    ! This interface is currently missing from the Fortran Libxc 5 interface
    subroutine xc_f03_available_functional_numbers_by_name(list) bind(c, name="xc_available_functional_numbers_by_name")
      use iso_c_binding
      import
      integer(c_int), intent(out) :: list(*)
    end subroutine xc_f03_available_functional_numbers_by_name
  end interface
#endif


                   !---Public/Private Statements---!

  private
  public :: set_parser_symbols

contains

  !-----------------------------------------------------------------------
  !> This subroutine defines some parser symbols. Basically they assign   
  !> parameters used in the code to possible values of input variables.   
  !-----------------------------------------------------------------------
  subroutine set_parser_symbols()

    integer :: ifunc
    character(len=:), allocatable :: known_func_names(:)
    integer, allocatable :: known_func_ids(:)

    ! CalculationMode
    call oct_parse_f90_putsym("ae",      AE)
    call oct_parse_f90_putsym("pp",      PP_GEN)
    call oct_parse_f90_putsym("pp_test", PP_TEST)
    call oct_parse_f90_putsym("nt",      NUM_TESTS)
    call oct_parse_f90_putsym("xc",      XC_EVAL)
    call oct_parse_f90_putsym("ip",      IP_CALC)

    ! EigensolverMode
    call oct_parse_f90_putsym("safe", EIGEN_SAFE)
    call oct_parse_f90_putsym("fast", EIGEN_FAST)

    ! PPScheme
    call oct_parse_f90_putsym("ham",   HAM)
    call oct_parse_f90_putsym("tm",    TM)
    call oct_parse_f90_putsym("rtm",   RTM)
    call oct_parse_f90_putsym("mrpp",  MRPP)
    call oct_parse_f90_putsym("rmrpp", RMRPP)
    call oct_parse_f90_putsym("mtm",   MTM)

    ! TheoryLevel
    call oct_parse_f90_putsym("independent_particles", INDEPENDENT_PARTICLES)
    call oct_parse_f90_putsym("dft",                   DFT)
    call oct_parse_f90_putsym("hartree_fock",          HARTREE_FOCK)
    call oct_parse_f90_putsym("hybrid",                HYBRID)

    ! WaveEquation
    call oct_parse_f90_putsym("schrodinger", SCHRODINGER)
    call oct_parse_f90_putsym("dirac",       DIRAC)
    call oct_parse_f90_putsym("scalar_rel",  SCALAR_REL)

    ! SpinMode
    call oct_parse_f90_putsym("unpolarized", 1)
    call oct_parse_f90_putsym("polarized",   2)

    ! MixingScheme
    call oct_parse_f90_putsym("linear",  MIX_LINEAR)
    call oct_parse_f90_putsym("broyden", MIX_BROYDEN)

    !MeshType
    call oct_parse_f90_putsym("linear", MESH_LINEAR)
    call oct_parse_f90_putsym("log1",   MESH_LOG1)
    call oct_parse_f90_putsym("log2",   MESH_LOG2)

    !MeshDerivMethod
    call oct_parse_f90_putsym("cubic_spline", MESH_CUBIC_SPLINE)
    call oct_parse_f90_putsym("finite_diff",  MESH_FINITE_DIFF)

    !SmearingFunction
    call oct_parse_f90_putsym("fixed_occ",       OCC_FIXED)
    call oct_parse_f90_putsym("semiconducting",  OCC_SEMICONDUCTING)
    call oct_parse_f90_putsym("averill_painter", OCC_AVERILL_PAINTER)

    ! XCFunctionals flags
    call oct_parse_f90_putsym("xc_deriv_method",   XC_DERIV_METHOD)
    call oct_parse_f90_putsym("xc_parameters",     XC_PARAMETERS)
    call oct_parse_f90_putsym("xc_dens_threshold", XC_DENS_THRESHOLD)

    ! XCFunctionals xc_deriv_method flag options
    call oct_parse_f90_putsym("xc_numerical", XC_DERIV_NUMERICAL)
    call oct_parse_f90_putsym("xc_analytical", XC_DERIV_ANALYTICAL)

    ! XC
    call oct_parse_f90_putsym("none", 0)

    ! LDAs
    call oct_parse_f90_putsym("lda_x",     XC_LDA_X)
    call oct_parse_f90_putsym("lda_x_rel", XC_LDA_X_REL)

#ifdef HAVE_LIBXC5
    allocate(character(xc_f03_number_of_functionals()) :: known_func_names(xc_f03_number_of_functionals()))
    allocate(known_func_ids(xc_f03_number_of_functionals()))

    call xc_f03_available_functional_numbers_by_name(known_func_ids)
    call xc_f03_available_functional_names(known_func_names)

    do ifunc = 1, xc_f03_number_of_functionals()
      call oct_parse_f90_putsym(trim(known_func_names(ifunc)), known_func_ids(ifunc))
    end do

    deallocate(known_func_names)
    deallocate(known_func_ids)
#else
    call oct_parse_f90_putsym("lda_c_wigner",   XC_LDA_C_WIGNER)
    call oct_parse_f90_putsym("lda_c_rpa",      XC_LDA_C_RPA)
    call oct_parse_f90_putsym("lda_c_hl",       XC_LDA_C_HL)
    call oct_parse_f90_putsym("lda_c_gl",       XC_LDA_C_GL)
    call oct_parse_f90_putsym("lda_c_xalpha",   XC_LDA_C_XALPHA)
    call oct_parse_f90_putsym("lda_c_vwn",      XC_LDA_C_VWN)
    call oct_parse_f90_putsym("lda_c_pz",       XC_LDA_C_PZ)
    call oct_parse_f90_putsym("lda_c_pz_mod",   XC_LDA_C_PZ_MOD)
    call oct_parse_f90_putsym("lda_c_ob_pz",    XC_LDA_C_OB_PZ)
    call oct_parse_f90_putsym("lda_c_pw",       XC_LDA_C_PW)
    call oct_parse_f90_putsym("lda_c_pw_mod",   XC_LDA_C_PW_MOD)
    call oct_parse_f90_putsym("lda_c_ob_pw",    XC_LDA_C_OB_PW)
    call oct_parse_f90_putsym("lda_c_vbh",      XC_LDA_C_vBH)
    call oct_parse_f90_putsym("lda_c_ml1",      XC_LDA_C_ML1)
    call oct_parse_f90_putsym("lda_c_ml2",      XC_LDA_C_ML2)
    call oct_parse_f90_putsym("lda_c_vwn_rpa",  XC_LDA_C_VWN_RPA)
    call oct_parse_f90_putsym("lda_c_gombas",   XC_LDA_C_GOMBAS)
    call oct_parse_f90_putsym("lda_c_pw_rpa",   XC_LDA_C_PW_RPA)
    call oct_parse_f90_putsym("lda_c_rc04",     XC_LDA_C_RC04)
    call oct_parse_f90_putsym("lda_c_vwn_1",    XC_LDA_C_VWN_1)
    call oct_parse_f90_putsym("lda_c_vwn_2",    XC_LDA_C_VWN_2)
    call oct_parse_f90_putsym("lda_c_vwn_3",    XC_LDA_C_VWN_3)
    call oct_parse_f90_putsym("lda_c_vwn_4",    XC_LDA_C_VWN_4)
    call oct_parse_f90_putsym("lda_c_chachiyo", XC_LDA_C_CHACHIYO)
    
    call oct_parse_f90_putsym("lda_xc_teter93", XC_LDA_XC_TETER93)
    call oct_parse_f90_putsym("lda_xc_zlp",     XC_LDA_XC_ZLP)
    
    call oct_parse_f90_putsym("lda_k_tf", XC_LDA_K_TF)
    call oct_parse_f90_putsym("lda_k_lp", XC_LDA_K_LP)
    
    ! GGAs
    call oct_parse_f90_putsym("gga_x_pbe",          XC_GGA_X_PBE)
    call oct_parse_f90_putsym("gga_x_pbe_r",        XC_GGA_X_PBE_R)
    call oct_parse_f90_putsym("gga_x_b86",          XC_GGA_X_B86)
    call oct_parse_f90_putsym("gga_x_b86_mgc",      XC_GGA_X_B86_MGC)
    call oct_parse_f90_putsym("gga_x_b88",          XC_GGA_X_B88)
    call oct_parse_f90_putsym("gga_x_g96",          XC_GGA_X_G96)
    call oct_parse_f90_putsym("gga_x_pw86",         XC_GGA_X_PW86)
    call oct_parse_f90_putsym("gga_x_pw91",         XC_GGA_X_PW91)
    call oct_parse_f90_putsym("gga_x_optx",         XC_GGA_X_OPTX)
    call oct_parse_f90_putsym("gga_x_dk87_r1",      XC_GGA_X_DK87_R1)
    call oct_parse_f90_putsym("gga_x_dk87_r2",      XC_GGA_X_DK87_R2)
    call oct_parse_f90_putsym("gga_x_lg93",         XC_GGA_X_LG93)
    call oct_parse_f90_putsym("gga_x_ft97_a",       XC_GGA_X_FT97_A)
    call oct_parse_f90_putsym("gga_x_ft97_b",       XC_GGA_X_FT97_B)
    call oct_parse_f90_putsym("gga_x_pbe_sol",      XC_GGA_X_PBE_SOL)
    call oct_parse_f90_putsym("gga_x_rpbe",         XC_GGA_X_RPBE )
    call oct_parse_f90_putsym("gga_x_wc",           XC_GGA_X_WC)
    call oct_parse_f90_putsym("gga_x_mpw91",        XC_GGA_X_mPW91)
    call oct_parse_f90_putsym("gga_x_am05",         XC_GGA_X_AM05)
    call oct_parse_f90_putsym("gga_x_pbea",         XC_GGA_X_PBEA)
    call oct_parse_f90_putsym("gga_x_mpbe",         XC_GGA_X_MPBE)
    call oct_parse_f90_putsym("gga_x_xpbe",         XC_GGA_X_XPBE)
    call oct_parse_f90_putsym("gga_x_bayesian",     XC_GGA_X_BAYESIAN)
    call oct_parse_f90_putsym("gga_x_pbe_jsjr",     XC_GGA_X_PBE_JSJR)
    call oct_parse_f90_putsym("gga_x_optb88_vdw",   XC_GGA_X_OPTB88_VDW)
    call oct_parse_f90_putsym("gga_x_pbek1_vdw",    XC_GGA_X_PBEK1_VDW)
    call oct_parse_f90_putsym("gga_x_optpbe_vdw",   XC_GGA_X_OPTPBE_VDW)
    call oct_parse_f90_putsym("gga_x_rge2",         XC_GGA_X_RGE2)
    call oct_parse_f90_putsym("gga_x_lb",           XC_GGA_X_LB)
    call oct_parse_f90_putsym("gga_x_rpw86",        XC_GGA_X_RPW86)
    call oct_parse_f90_putsym("gga_x_kt1",          XC_GGA_X_KT1)
    call oct_parse_f90_putsym("gga_x_herman",       XC_GGA_X_HERMAN)
    call oct_parse_f90_putsym("gga_x_apbe",         XC_GGA_X_APBE)
    call oct_parse_f90_putsym("gga_x_mb88",         XC_GGA_X_MB88)
    call oct_parse_f90_putsym("gga_x_ol2",          XC_GGA_X_OL2)
    call oct_parse_f90_putsym("gga_x_lbm",          XC_GGA_X_LBM)
    call oct_parse_f90_putsym("gga_x_airy",         XC_GGA_X_AIRY)
    call oct_parse_f90_putsym("gga_x_lag",          XC_GGA_X_LAG)
    call oct_parse_f90_putsym("gga_x_htbs",         XC_GGA_X_HTBS)
    call oct_parse_f90_putsym("gga_x_sogga",        XC_GGA_X_SOGGA)
    call oct_parse_f90_putsym("gga_x_sogga11",      XC_GGA_X_SOGGA11)
    call oct_parse_f90_putsym("gga_x_c09x",         XC_GGA_X_C09X)
    call oct_parse_f90_putsym("gga_x_ssb_sw",       XC_GGA_X_SSB_SW)
    call oct_parse_f90_putsym("gga_x_ssb",          XC_GGA_X_SSB)
    call oct_parse_f90_putsym("gga_x_ssb_d",        XC_GGA_X_SSB_D)
    call oct_parse_f90_putsym("gga_x_bpccac",       XC_GGA_X_BPCCAC)
    call oct_parse_f90_putsym("gga_x_wpbeh",        XC_GGA_X_WPBEH)
    call oct_parse_f90_putsym("gga_x_hjs_pbe",      XC_GGA_X_HJS_PBE)
    call oct_parse_f90_putsym("gga_x_hjs_pbe_sol",  XC_GGA_X_HJS_PBE_SOL)
    call oct_parse_f90_putsym("gga_x_hjs_b88",      XC_GGA_X_HJS_B88)
    call oct_parse_f90_putsym("gga_x_hjs_b97x",     XC_GGA_X_HJS_B97X)
    call oct_parse_f90_putsym("gga_x_ityh",         XC_GGA_X_ITYH)
    call oct_parse_f90_putsym("gga_x_lv_rpw86",     XC_GGA_X_LV_RPW86)
    call oct_parse_f90_putsym("gga_x_n12",          XC_GGA_X_N12)
    call oct_parse_f90_putsym("gga_x_vmt_ge",       XC_GGA_X_VMT_GE)
    call oct_parse_f90_putsym("gga_x_vmt_pbe",      XC_GGA_X_VMT_PBE)
    call oct_parse_f90_putsym("gga_x_vmt84_pbe",    XC_GGA_X_VMT84_PBE)
    call oct_parse_f90_putsym("gga_x_vmt84_ge",     XC_GGA_X_VMT84_GE)
    call oct_parse_f90_putsym("gga_x_pbeint",       XC_GGA_X_PBEINT)
    call oct_parse_f90_putsym("gga_x_tca",          XC_GGA_X_PBE_TCA)
    call oct_parse_f90_putsym("gga_x_sfat",         XC_GGA_X_SFAT)
    call oct_parse_f90_putsym("gga_x_ak13",         XC_GGA_X_AK13)
    call oct_parse_f90_putsym("gga_x_lambda_lo_n",  XC_GGA_X_LAMBDA_LO_N)
    call oct_parse_f90_putsym("gga_x_lambda_ch_n",  XC_GGA_X_LAMBDA_CH_N)
    call oct_parse_f90_putsym("gga_x_lambda_oc2_n", XC_GGA_X_LAMBDA_OC2_N)
    call oct_parse_f90_putsym("gga_x_ev93",         XC_GGA_X_EV93)
    call oct_parse_f90_putsym("gga_x_gam",          XC_GGA_X_GAM)
    call oct_parse_f90_putsym("gga_x_pbefe",        XC_GGA_X_PBEFE)
    call oct_parse_f90_putsym("gga_x_eb88",         XC_GGA_X_EB88)
    call oct_parse_f90_putsym("gga_x_beefvdw",      XC_GGA_X_BEEFVDW)
    call oct_parse_f90_putsym("gga_x_bcgp",         XC_GGA_X_BCGP)
    
    call oct_parse_f90_putsym("gga_c_pbe",          XC_GGA_C_PBE)
    call oct_parse_f90_putsym("gga_c_lyp",          XC_GGA_C_LYP)
    call oct_parse_f90_putsym("gga_c_p86",          XC_GGA_C_P86)
    call oct_parse_f90_putsym("gga_c_pbe_sol",      XC_GGA_C_PBE_SOL)
    call oct_parse_f90_putsym("gga_c_pw91",         XC_GGA_C_PW91)
    call oct_parse_f90_putsym("gga_c_am05",         XC_GGA_C_AM05)
    call oct_parse_f90_putsym("gga_c_xpbe",         XC_GGA_C_XPBE)
    call oct_parse_f90_putsym("gga_c_lm",           XC_GGA_C_LM)
    call oct_parse_f90_putsym("gga_c_pbe_jrgx",     XC_GGA_C_PBE_JRGX)
    call oct_parse_f90_putsym("gga_c_rge2",         XC_GGA_C_RGE2)
    call oct_parse_f90_putsym("gga_c_wl",           XC_GGA_C_WL)
    call oct_parse_f90_putsym("gga_c_wi",           XC_GGA_C_WI)
    call oct_parse_f90_putsym("gga_c_apbe",         XC_GGA_C_APBE)
    call oct_parse_f90_putsym("gga_c_sogga11",      XC_GGA_C_SOGGA11)
    call oct_parse_f90_putsym("gga_c_sogga11_x",    XC_GGA_C_SOGGA11_X)
    call oct_parse_f90_putsym("gga_c_wi0",          XC_GGA_C_WI0)
    call oct_parse_f90_putsym("gga_c_op_xalpha",    XC_GGA_C_OP_XALPHA)
    call oct_parse_f90_putsym("gga_c_op_g96",       XC_GGA_C_OP_G96)
    call oct_parse_f90_putsym("gga_c_op_pbe",       XC_GGA_C_OP_PBE)
    call oct_parse_f90_putsym("gga_c_op_b88",       XC_GGA_C_OP_B88)
    call oct_parse_f90_putsym("gga_c_ft97",         XC_GGA_C_FT97)
    call oct_parse_f90_putsym("gga_c_spbe",         XC_GGA_C_SPBE)
    call oct_parse_f90_putsym("gga_c_revtca",       XC_GGA_C_REVTCA)
    call oct_parse_f90_putsym("gga_c_tca",          XC_GGA_C_TCA)
    call oct_parse_f90_putsym("gga_c_optc",         XC_GGA_C_OPTC)
    call oct_parse_f90_putsym("gga_c_vpbe",         XC_GGA_C_VPBE)
    call oct_parse_f90_putsym("gga_c_pbeint",       XC_GGA_C_PBEINT)
    call oct_parse_f90_putsym("gga_c_zpbesol",      XC_GGA_C_ZPBESOL)
    call oct_parse_f90_putsym("gga_c_zpbeint",      XC_GGA_C_ZPBEINT)
    call oct_parse_f90_putsym("gga_c_hcth_a",       XC_GGA_C_HCTH_A)
    call oct_parse_f90_putsym("gga_c_gam",          XC_GGA_C_GAM)
    call oct_parse_f90_putsym("gga_c_pbefe",        XC_GGA_C_PBEFE)
    call oct_parse_f90_putsym("gga_c_op_pw91",      XC_GGA_C_OP_PW91)
    call oct_parse_f90_putsym("gga_c_pbe_mol",      XC_GGA_C_PBE_MOL)
    call oct_parse_f90_putsym("gga_c_bmk",          XC_GGA_C_BMK)
    call oct_parse_f90_putsym("gga_c_tau_hcth",     XC_GGA_C_TAU_HCTH)
    call oct_parse_f90_putsym("gga_c_hyb_tau_hcth", XC_GGA_C_HYB_TAU_HCTH)
    call oct_parse_f90_putsym("gga_c_bcgp",         XC_GGA_C_BCGP)

    call oct_parse_f90_putsym("gga_xc_hcth_93",   XC_GGA_XC_HCTH_93 )
    call oct_parse_f90_putsym("gga_xc_hcth_120",  XC_GGA_XC_HCTH_120)
    call oct_parse_f90_putsym("gga_xc_hcth_147",  XC_GGA_XC_HCTH_147)
    call oct_parse_f90_putsym("gga_xc_hcth_407",  XC_GGA_XC_HCTH_407)
    call oct_parse_f90_putsym("gga_xc_edf1",      XC_GGA_XC_EDF1)
    call oct_parse_f90_putsym("gga_xc_xlyp",      XC_GGA_XC_XLYP)
    call oct_parse_f90_putsym("gga_xc_pbe1w",     XC_GGA_XC_PBE1W)
    call oct_parse_f90_putsym("gga_xc_mpwlyp1w",  XC_GGA_XC_MPWLYP1W)
    call oct_parse_f90_putsym("gga_xc_pbelyp1w",  XC_GGA_XC_PBELYP1W)
    call oct_parse_f90_putsym("gga_xc_kt2",       XC_GGA_XC_KT2)
    call oct_parse_f90_putsym("gga_xc_th1",       XC_GGA_XC_TH1)
    call oct_parse_f90_putsym("gga_xc_th2",       XC_GGA_XC_TH2)
    call oct_parse_f90_putsym("gga_xc_th3",       XC_GGA_XC_TH3)
    call oct_parse_f90_putsym("gga_xc_th4",       XC_GGA_XC_TH4)
    call oct_parse_f90_putsym("gga_xc_mohlyp",    XC_GGA_XC_MOHLYP)
    call oct_parse_f90_putsym("gga_xc_mohlyp2",   XC_GGA_XC_MOHLYP2)
    call oct_parse_f90_putsym("gga_xc_th_fl",     XC_GGA_XC_TH_FL)
    call oct_parse_f90_putsym("gga_xc_th_fc",     XC_GGA_XC_TH_FC)
    call oct_parse_f90_putsym("gga_xc_th_fcfo",   XC_GGA_XC_TH_FCFO)
    call oct_parse_f90_putsym("gga_xc_th_fco",    XC_GGA_XC_TH_FCO)
    call oct_parse_f90_putsym("gga_xc_hcth_407p", XC_GGA_XC_HCTH_407P)
    call oct_parse_f90_putsym("gga_xc_hcth_p76",  XC_GGA_XC_HCTH_P76)
    call oct_parse_f90_putsym("gga_xc_hcth_p14",  XC_GGA_XC_HCTH_P14)
    call oct_parse_f90_putsym("gga_xc_b97_gga1",  XC_GGA_XC_B97_GGA1)
    call oct_parse_f90_putsym("gga_xc_oblyp_d",   XC_GGA_XC_OBLYP_D)
    call oct_parse_f90_putsym("gga_xc_opwlyp_d",  XC_GGA_XC_OPWLYP_D)
    call oct_parse_f90_putsym("gga_xc_opbe_d",    XC_GGA_XC_OPBE_D)
    call oct_parse_f90_putsym("gga_xc_beefvdw",   XC_GGA_XC_BEEFVDW)
    
    call oct_parse_f90_putsym("gga_k_vw",        XC_GGA_K_VW)
    call oct_parse_f90_putsym("gga_k_ge2",       XC_GGA_K_GE2)
    call oct_parse_f90_putsym("gga_k_golden",    XC_GGA_K_GOLDEN)
    call oct_parse_f90_putsym("gga_k_yt65",      XC_GGA_K_YT65)
    call oct_parse_f90_putsym("gga_k_baltin",    XC_GGA_K_BALTIN)
    call oct_parse_f90_putsym("gga_k_lieb",      XC_GGA_K_LIEB)
    call oct_parse_f90_putsym("gga_k_absr1",     XC_GGA_K_ABSR1)
    call oct_parse_f90_putsym("gga_k_absr2",     XC_GGA_K_ABSR2)
    call oct_parse_f90_putsym("gga_k_gr",        XC_GGA_K_GR)
    call oct_parse_f90_putsym("gga_k_ludena",    XC_GGA_K_LUDENA)
    call oct_parse_f90_putsym("gga_k_gp85",      XC_GGA_K_GP85)
    call oct_parse_f90_putsym("gga_k_pearson",   XC_GGA_K_PEARSON)
    call oct_parse_f90_putsym("gga_k_llp",       XC_GGA_K_LLP)
    call oct_parse_f90_putsym("gga_k_ol1",       XC_GGA_K_OL1)
    call oct_parse_f90_putsym("gga_k_ol2",       XC_GGA_K_OL2)
    call oct_parse_f90_putsym("gga_k_fr_b88",    XC_GGA_K_FR_B88)
    call oct_parse_f90_putsym("gga_k_fr_pw86",   XC_GGA_K_FR_PW86)
    call oct_parse_f90_putsym("gga_k_dk",        XC_GGA_K_DK)
    call oct_parse_f90_putsym("gga_k_perdew",    XC_GGA_K_PERDEW)
    call oct_parse_f90_putsym("gga_k_vsk",       XC_GGA_K_VSK)
    call oct_parse_f90_putsym("gga_k_vjks",      XC_GGA_K_VJKS)
    call oct_parse_f90_putsym("gga_k_ernzerhof", XC_GGA_K_ERNZERHOF)
    call oct_parse_f90_putsym("gga_k_lc94",      XC_GGA_K_LC94)
    call oct_parse_f90_putsym("gga_k_thakkar",   XC_GGA_K_THAKKAR)
    call oct_parse_f90_putsym("gga_k_apbe",      XC_GGA_K_APBE)
    call oct_parse_f90_putsym("gga_k_tw1",       XC_GGA_K_TW1)
    call oct_parse_f90_putsym("gga_k_tw2",       XC_GGA_K_TW2)
    call oct_parse_f90_putsym("gga_k_tw3",       XC_GGA_K_TW3)
    call oct_parse_f90_putsym("gga_k_tw4",       XC_GGA_K_TW4)
    call oct_parse_f90_putsym("gga_k_meyer",     XC_GGA_K_MEYER)
    call oct_parse_f90_putsym("gga_k_absp3",     XC_GGA_K_ABSP3)
    call oct_parse_f90_putsym("gga_k_absp4",     XC_GGA_K_ABSP4)

    ! metaGGAs
    call oct_parse_f90_putsym("mgga_x_lta",      XC_MGGA_X_LTA)
    call oct_parse_f90_putsym("mgga_x_tpss",     XC_MGGA_X_TPSS)
    call oct_parse_f90_putsym("mgga_x_m06_l",    XC_MGGA_X_M06_L)
    call oct_parse_f90_putsym("mgga_x_gvt4",     XC_MGGA_X_GVT4)
    call oct_parse_f90_putsym("mgga_x_tau_hcth", XC_MGGA_X_TAU_HCTH)
    call oct_parse_f90_putsym("mgga_x_br89",     XC_MGGA_X_BR89)
    call oct_parse_f90_putsym("mgga_x_bj06",     XC_MGGA_X_BJ06)
    call oct_parse_f90_putsym("mgga_x_tb09",     XC_MGGA_X_TB09)
    call oct_parse_f90_putsym("mgga_x_rpp09",    XC_MGGA_X_RPP09)
    call oct_parse_f90_putsym("mgga_x_revtpss",  XC_MGGA_X_REVTPSS)
    call oct_parse_f90_putsym("mgga_x_pkzb",     XC_MGGA_X_PKZB)
    call oct_parse_f90_putsym("mgga_x_ms0",      XC_MGGA_X_MS0)
    call oct_parse_f90_putsym("mgga_x_ms1",      XC_MGGA_X_MS1)
    call oct_parse_f90_putsym("mgga_x_ms2",      XC_MGGA_X_MS2)
    call oct_parse_f90_putsym("mgga_x_mn12_l",   XC_MGGA_X_MN12_L)
    call oct_parse_f90_putsym("mgga_x_mk00",     XC_MGGA_X_MK00)
    call oct_parse_f90_putsym("mgga_x_mk00b",    XC_MGGA_X_MK00B)
    call oct_parse_f90_putsym("mgga_x_bloc",     XC_MGGA_X_BLOC)
    call oct_parse_f90_putsym("mgga_x_modtpss",  XC_MGGA_X_MODTPSS)
    call oct_parse_f90_putsym("mgga_x_mbeef",    XC_MGGA_X_MBEEF)
    call oct_parse_f90_putsym("mgga_x_mbeffvdw", XC_MGGA_X_MBEEFVDW)
    call oct_parse_f90_putsym("mgga_x_mvs",      XC_MGGA_X_MVS)
    call oct_parse_f90_putsym("mgga_x_scan",     XC_MGGA_X_SCAN)
    call oct_parse_f90_putsym("mgga_x_b00",      XC_MGGA_X_B00)

    call oct_parse_f90_putsym("mgga_c_tpss",    XC_MGGA_C_TPSS)
    call oct_parse_f90_putsym("mgga_c_vsxc",    XC_MGGA_C_VSXC)
    call oct_parse_f90_putsym("mgga_c_m06_l",   XC_MGGA_C_M06_L)
    call oct_parse_f90_putsym("mgga_c_m06_hf",  XC_MGGA_C_M06_HF)
    call oct_parse_f90_putsym("mgga_c_m06",     XC_MGGA_C_M06)
    call oct_parse_f90_putsym("mgga_c_m06_2x",  XC_MGGA_C_M06_2X)
    call oct_parse_f90_putsym("mgga_c_m05",     XC_MGGA_C_M05)
    call oct_parse_f90_putsym("mgga_c_m05_2x",  XC_MGGA_C_M05_2X)
    call oct_parse_f90_putsym("mgga_c_pkzb",    XC_MGGA_C_PKZB)
    call oct_parse_f90_putsym("mgga_c_bc95",    XC_MGGA_C_BC95)
    call oct_parse_f90_putsym("mgga_c_m08_hx",  XC_MGGA_C_M08_HX)
    call oct_parse_f90_putsym("mgga_c_m08_so",  XC_MGGA_C_M08_SO)
    call oct_parse_f90_putsym("mgga_c_m11",     XC_MGGA_C_M11)
    call oct_parse_f90_putsym("mgga_c_m11_l",   XC_MGGA_C_M11_L)
    call oct_parse_f90_putsym("mgga_c_mn12_l",  XC_MGGA_C_MN12_L)
    call oct_parse_f90_putsym("mgga_c_mn12_sx", XC_MGGA_C_MN12_SX)
    call oct_parse_f90_putsym("mgga_c_revtpss", XC_MGGA_C_REVTPSS)
    call oct_parse_f90_putsym("mgga_c_tpssloc", XC_MGGA_C_TPSSLOC)
    call oct_parse_f90_putsym("mgga_c_dldf",    XC_MGGA_C_DLDF)
    call oct_parse_f90_putsym("mgga_c_mn15_l",  XC_MGGA_C_MN15_L)
    call oct_parse_f90_putsym("mgga_c_scan",    XC_MGGA_C_SCAN)

    call oct_parse_f90_putsym("mgga_xc_otpss_d",   XC_MGGA_XC_OTPSS_D)
    call oct_parse_f90_putsym("mgga_xc_tpsslyp1w", XC_MGGA_XC_TPSSLYP1W)
    call oct_parse_f90_putsym("mgga_xc_zlp",       XC_MGGA_XC_ZLP)
    call oct_parse_f90_putsym("mgga_xc_b97m_v",    XC_MGGA_XC_B97M_V)
    call oct_parse_f90_putsym("mgga_xc_cc06",      XC_MGGA_XC_CC06)
#endif

    ! This kinetic energy MGGA is not in Libxc
    call oct_parse_f90_putsym("mgga_k_ge2", XC_MGGA_K_GE2)

    ! XC corrections
    call oct_parse_f90_putsym("adsic", XC_CORRECTION_ADSIC)
    call oct_parse_f90_putsym("rhoxc", XC_CORRECTION_RHOXC)

    ! PPOutputFileFormat
    call oct_parse_f90_putsym("siesta",    PSIO_SIESTA   )
    call oct_parse_f90_putsym("fhi",       PSIO_FHI      )
    call oct_parse_f90_putsym("abinit5",   PSIO_ABINIT5  )
    call oct_parse_f90_putsym("abinit6",   PSIO_ABINIT6  )
    call oct_parse_f90_putsym("upf",       PSIO_UPF      )
    call oct_parse_f90_putsym("parsec",    PSIO_PARSEC   )
    call oct_parse_f90_putsym("latepp_so", PSIO_LATEPP_SO)

    ! ODESteppingFunction
    call oct_parse_f90_putsym("rk2",   ODE_RK2  )
    call oct_parse_f90_putsym("rk4",   ODE_RK4  )
    call oct_parse_f90_putsym("rkf4",  ODE_RKF4 )
    call oct_parse_f90_putsym("rkck4", ODE_RKCK4)
    call oct_parse_f90_putsym("rkpd8", ODE_RKPD8)

    ! PPTests
    call oct_parse_f90_putsym("ld",      TEST_LD)
    call oct_parse_f90_putsym("dm",      TEST_DM)
    call oct_parse_f90_putsym("ip_test", TEST_IP)
    call oct_parse_f90_putsym("ee",      TEST_EE)


    ! Numerical tests
    call oct_parse_f90_putsym("derivatives", TEST_DERIVATIVES)
    call oct_parse_f90_putsym("integration", TEST_INTEGRATION)
    call oct_parse_f90_putsym("hartree",     TEST_HARTREE)

    ! Test functions
    call oct_parse_f90_putsym("gaussian",    GAUSSIAN)
    call oct_parse_f90_putsym("inverse",     INVERSE)
    call oct_parse_f90_putsym("exponential", EXPONENTIAL)

  end subroutine set_parser_symbols

end module parser_symbols_m
