# Installation

## Quick instructions

If you are feeling lucky:

```sh
tar xzf ape<-version>.tar.gz
cd ape-<version>
./configure
make
make install
```

This may not work but, before giving up, just read the following
paragraphs.


## Long instructions


The code is written in standard Fortran 2003, with some routines written in
C. To build it you will need both a C compiler (`gcc` works just fine), and a
Fortran 2003 compiler (`gfortran` also works fine).

Besides the compiler, you will also need:

1.  **make**: most computers have it installed, otherwise just grab and
    install the GNU make.

2.  **cpp**: GNU cpp is just fine, but any `cpp` that accepts the `-C`
    flag (preserve comments) should work just as well.

3.  **GSL**: The GSL is heavily used in APE. If you don’t have it already
    installed in your system, you can obtain GSL from
    [here](http://sources.redhat.com/gsl/). You will need version 1.0 or higher.

4.  **Libxc**: Libxc is a library of exchange and correlation functionals. If
    you don’t have it already installed in your system, you can obtain it
    [here](https://libxc.gitlab.io).

First you should obtain the code file, `ape<-version>.tar.gz`, (you probably
have already done this). The code is freely available and can be downloaded from
[Gitlab](https://gitlab.com/ape/ape/-/releases). Note that there are several
files listed for each release - you need to download the one named
`ape<-version>.tar.gz`, not the one named `Source code (tar.gz)`.

Now uncompress and untar the file:

```sh
tar xzf ape<-version>.tar.gz
```

In the following, `APE-HOME` denotes the home directory of APE, created by the
tar command.

`APE-HOME` contains the following subdirectories:

- `autom4te.cache`, `build`: contains files related to the building system. May
  actually not be there. Not of real interest for the plain user.

- `doc`: The documentation of APE in *Markdown* format.

- `liboct_parser`: The parser used by APE. This is the parser from
  `Octopus`.

- `sample`: Sample input files for APE.

- `src`: Fortran 2003 source files. Note that these have to be preprocessed
  before being fed to the Fortran compiler, so do not be scared by the \#
  directives.

- `testsuite`: APE regression testsuite.

Before configuring you can (should) setup a couple of options. Although the
configure script tries to guess your system settings for you, we recommend that
you set explicitly the default Fortran 2003 compiler and the compiler
options. For example, in bash you would typically do:
```sh
export FC=gfortran
export FCFLAGS="-O3 -Wall"
```

if you are using the GNU Fortran compiler on a Linux machine. Also, if you have
some of the required libraries in some unusual directories, these directories
may be placed in the variable `LDFLAGS` (e.g., `export LDFLAGS="-L/opt/lib/"`).

You can now run the configure script<sup id="note1">[1](#f1)</sup>
```sh
./configure
```

There are some options you can use with the configure script. To obtain a full
list just type
```sh
./configure --help
```

Some commonly used options include:

- `--prefix=PREFIX`: Change the base installation dir of APE to `PREFIX`. The
    executable will be installed in `PREFIX/bin`, the libraries in `PREFIX/lib`
    and the documentation in `PREFIX/info`.  `PREFIX` defaults to the home
    directory of the user who runs `configure`.

- `--with-gsl-prefix=DIR`: Installation directory of the GSL library. The
    libraries are expected to be in `DIR/lib` and the include files in
    `DIR/include`. The value of `DIR` is usually found by issuing the command
    `gsl-config --prefix`. (If the GSL library is installed, the program
    `gsl-config` should be somewhere.)

- `--with-libxc-prefix=DIR`: Installation directory of the Libxc library. The
    libraries are expected to be in `DIR/lib` and the include files in
    `DIR/include`.

Then compile the code and launch the testsuite by running
```sh
make
make check
```

If all tests were passed, you can then install the code with
```sh
make install
```

If everything went fine, you should now be able to run APE.


## Troubleshooting

So, something went wrong. Here is a list of things that might have gone wrong.

**Some tests of the testsuite fail**: When running the testsuite, one or more
tests fail.

- Is the test failing because of a very small numerical difference?  While
  running a test, if a test case is failed, the code will print some extra
  information about the failure, namely the calculated value, the reference
  value, and the allowed tolerance. This should look like this:

          Match Failed
           Calculated value : -24.344302
           Reference value  : -24.344198
           Difference       : 0.000104
           Tolerance        : 8e-5

  If the difference is slightly larger than the tolerance, like in the previous
  example, and if the same happens for all the failed test cases, then it is
  likely that there is nothing wrong with your compilation.

**Could not find GSL library**: We assume that you have already installed GSL
but, for some reason, you were not able to compile the code.

- Did you pass the correct `--with-gsl-prefix` to the configure script? If your
  library is installed in a non-standard directory (like `/opt/gsl`), you will
  have to pass the script the location of the library (in this example, you
  could try `./configure --with-gsl-prefix=/opt/gsl`.

**Could not find Libxc library**: We assume that you have already installed
libxc but, for some reason, you were not able to compile the code.

- Did you pass the correct `--with-libxc-prefix` to the configure script? If
  your library is installed in a non-standard directory (like `/opt/libxc`), you
  will have to pass the script the location of the library (in this example, you
  could try `./configure --with-libxc-prefix=/opt/libxc`.

- Did you use the same Fortran compiler when compiling Libxc? Unfortunately the
  compilation of Fortran modules generates compiler dependent files (usually
  with the `.mod` extension). Since APE uses the Fortran interface of Libxc, it
  is required that you use the same Fortran compiler for APE and Libxc.

**Error while loading shared libraries**: You have already compiled the
code, but it fails to run, giving an error message about a failure in
locating some shared object file.

- Did you set the `LD_LIBRARY_PATH` environment variable? If some of the
  libraries used to compile APE are installed in a non-standard directory (like
  `/opt/libxc`), you will have to add the path to the directory where the
  library objects are installed to the `LD_LIBRARY_PATH` environment
  variable. If you are using bash, this is done with the following command:

```sh
export LD_LIBRARY_PATH=/path/to/libxc/lib:$LD_LIBRARY_PATH
```

**Fatal Error**: Sometimes the code stops after issuing a *Fatal Error*
message. Here is a list of some of the most common errors and a brief
explanation of their origin and some possible solutions.

- *Unable to bracket eigenvalue for state xxx.* This error occurs when the
  eigensolver is unable to find the eigenvalue for a given orbital. Most of the
  times this will happen when an occupied orbital becomes unbound.

- *Generation of pseudopotentials from spin-polarized calculations is not
  implemented!* You are trying to generate a pseudopotential from a
  spin-polarized calculation, but this is not possible in APE.  Although there
  are schemes to generate spin-dependent pseudopotentials, for most
  applications, spin-polarized calculations can be performed using
  pseudopotentials generated for a spin-unpolarized atom. This is because
  spin-polarization should come from the valence electrons. Furthermore,
  spin-dependent pseudopotentials cannot be used in the same way as
  spin-independent ones and most DFT codes don’t know how to handle them.

**Whatever went wrong...**: If something else went wrong, please create an issue
on [Gitlab](https://gitlab.com/ape/ape/).


---

<b id="f1">[1]</b> If you downloaded the git version, you will not find the
`configure` script. In order to compile the development version you will first
have to run the GNU autotools. This may be done by executing the command
`autoreconf -i`. Note that you need to have working versions of the `automake`
(1.11), `autoconf` (2.68) and `libtool` (2.4) programs (the versions we
currently use are between parentheses). Note that `autoreconf` will likely not
work if you have (much) older versions of the autotools.[↩](#note1)
