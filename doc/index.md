---
hide:
  - toc
---

# What is APE?

The Atomic Pseudopotential Engine (APE) is a computer package designed to
generate and test norm-conserving pseudopotentials within Density Functional
Theory. The generated pseudopotentials can be either non-relativistic, scalar
relativistic or fully relativistic and can explicitly include semi-core
states. A wide range of exchange-correlation functionals is included through
[Libxc](https://libxc.gitlab.io).

APE can output the pseudopotentials in a variety of file formats suitable to use
in many of the most popular DFT codes. These include (in no particular order):

- [Siesta](https://departments.icmab.es/leem/siesta/)
- [Octopus](http://octopus-code.org)
- [ABINIT](https://www.abinit.org/)
- [Quantum-Espresso](http://www.quantum-espresso.org/)


APE is written in Fortran and is distributed under the
[GPLv2](https://www.gnu.org/licenses/gpl-2.0.en.html). Contributions are
welcome.

More details about APE can be found in the manual and the following article:
[M. Oliveira and F. Nogueira, Comput. Phys. Comm. 178, 524
(2008)](http://dx.doi.org/10.1016/j.cpc.2007.11.003).


Questions about the code, bug reports and patches should be submited over
[Gitlab](http://gitlab.com/ape/ape).
