# Input

APE has quite a few options, that we will subdivide in different groups. After
the name of the option, its type and default value (when applicable) are also
given. Do not be scared by the amount of options! For many calculations you can
safely rely on the default values of most of the options.

## The parser


All input options should be in a file called `inp.ape`, in the directory APE is
run from. It is also possible to use a file with a different name. In that case
the standard input should redirect to that file. For example, in bash:

```sh
ape < filename
```

For fairly comprehensive examples, just look at the files in the
`APE_HOME/samples` directory.

At the beginning of the program the oct_parser library reads the `inp.ape` file,
parses it, and generates a list of variables that will be read by APE (note that
the input is case independent). There are two kind of variables, scalar values
(strings or numbers), and blocks (that you may view as matrices). A scalar
variable `var` can be defined by:
```
var = exp
```

`var` can contain any alphanumeric character plus "\_", and `exp` can be a quote
delimited string, a number (integer, real, or complex), a variable name, or a
mathematical expression. In the expressions all arithmetic operators are
supported (`a+b`, `a-b`, `a*b`, `a/b`; for exponentiation the C syntax `a^b`
is used), and the following functions can be used:

- `sqrt(x)`: The square root of `x`.

- `exp(x)`: The exponential of `x`.

- `log(x)` or `ln(x)`: The natural logarithm of `x`.

- `log10(x)`: Base 10 logarithm of `x`.

- `sin(x)`, `cos(x)`, `tan(x)`, `cot(x)`, `sec(x)`, `csc(x)`: The sinus,
  co-sinus, tangent, co-tangent, secant and co-secant of `x`.

- `asin(x)`, `acos(x)`, `atan(x)`, `acot(x)`, `asec(x)`, `acsc(x)`: The inverse
  (arc-) sinus, co-sinus, tangent, co-tangent, secant and co-secant of `x`.

- `sinh(x)`, `cosh(x)`, `tanh(x)`, `coth(x)`, `sech(x)`, `csch(x)`: The
  hyperbolic sinus, co-sinus, tangent, co-tangent, secant and co-secant of `x`.

- `asinh(x)`, `acosh(x)`, `atanh(x)`, `acoth(x)`, `asech(x)`, `acsch(x)`: The
  inverse hyperbolic sinus, co-sinus, tangent, co-tangent, secant and co-secant
  of `x`.

You can also use any of the predefined variables:

- `pi`: 3.141592653589793, what else is there to say?

- `e`: The base of the natural logarithms.

- `false` or `f` or `no`: False in all its flavors. For the curious, `false` is
  defined as 0.

- `true` or `t` or `yes`: The truthful companion of `false`. For the curious,
  `true` is defined as 1.

Blocks are defined as a collection of values, organized in row and column
format. The syntax is the following:

```
%var
  exp | exp | exp | ...
  exp | exp | exp | ...
 ...
%
```

Rows in a block are separated by a newline, while columns are separated by the
character "|" or by a tab. There may be any number of lines and any number of
columns in a block. Note also that each line can have a different number of
columns.

If APE tries to read a variable that is not defined in the `inp.ape` file, it
automatically assigns to it a default value. All variables read are output to
the file `parser.log`. If you are not sure of what the program is reading, just
take a look at it. Everything following the character "\#" until the end of the
line is considered a comment and is simply cast into oblivion.



## Generalities


### CalculationMode
##### Type: flag
##### Default: ae+pp

It defines the type of calculation to perform. Options are:

- `ae`: Atomic calculation.

- `pp`: Pseudopotential generation.

- `pp_test`: Pseudopotential test.

- `xc`: One-shot evaluation of exchange and correlation energies and potentials
  and/or kinetic energy density functionals.

- `ip`: Calculation of ionization energy.

- `nt`: Numerical tests. Should only be useful to developers.

### Units
##### Type: integer
##### Default: 1

Internally, the code works in atomic units. However, you can use this option to
define the units used in the input and output files.

Valid options are:

- `1`: atomic units.

- `2`: atomic Rydberg units.

- `3`: electron-volts/ångström.

### UnitsOutput
##### Type: integer
##### Default: 1

Same as `Units`, but only refers to the values in the output files.

### UnitsInput
##### Type: integer
##### Default: 1

Same as `Units`, but only refers to the values in the input file.

### Verbose
##### Type: integer
##### Default: 30

Verbosity level of the program. The higher, the more verbose APE is.
Current levels are:

- `verbose` $\le 0$: Silent mode. No output except fatal errors.

- `verbose` $\gt 0$: Warnings only.

- `verbose` $\gt 10$: Important program info only.

- `verbose` $\gt 20$: Normal program info.

- `verbose` $\gt 30$: Normal program info plus detailed info about the SCF
    cycle.




## Hamiltonian


These parameters control what Hamiltonian to use when solving the Khon-Sham
equations.

### WaveEquation
##### Type: integer
##### Default: schrodinger

When performing atomic calculations APE can solve either the Kohn-Sham
equations, the Dirac-Kohn-Sham equations or the scalar-relativistic Khon-Sham
equations. Valid options are:

- `schrodinger`: Kohn-Sham equations.

- `dirac`: Dirac-Kohn-Sham equations.

- `scalar_rel`: scalar-relativistic Kohn-Sham equations.

### SpinMode
##### Type: integer
##### Default: unpolarized

Defines the spin mode APE will run in. Valid modes are:

- `unpolarized`: Spin-unpolarized calculation.

- `polarized`: Spin-polarized calculation.

### XCFunctionals
##### Type: block data

This block describes the XC functionals to be used. Up to two functionals can be
specified. The format for each line is the following:

```no-lang
%XCFunctionals
  id | flag1 | value1 | flag2 | value2 | ...
%
```

The `id` is the functional id from Libxc. A full list of available functional
can be found on the [Libxc website](https://libxc.gitlab.io/).
Note that not all functionals will work with APE. Depending on the selected
functional, some further options might be available. To specify those, one first
needs a flag to tell APE the meaning of the following values. Available flags
are:

- `xc_dens_threshold`: This value sets the Libxc density threshold. For any
  values of the density lower than this threshold, Libxc sets the energy per
  unit particle and the potential to zero.

- `xc_deriv_method`: If the xc potential is to be obtained using numerical
  (`xc_numerical`) or analytical (`xc_analytical`) derivatives. Note that
  analytical derivatives are only available for LDA and GGA functionals.

- `xc_parameters`: Allows to set parameters required by some functionals.

Here is an example on how to use these flags:

```no-lang
%XCFunctionals
  lda_x | xc_dens_threshold | 1e-12 | xc_deriv_method | xc_analytical
  lda_c_xalpha | xc_parameters | 0.1
%
```

If no `XCFunctionals` block is specified, APE will use the following default:

```no-lang
%XCFunctionals
 lda_x
 lda_c_pw
%
```

### XCCorrections
##### Type: integer
##### Default: none

The possible values are:

- `none`: no corrections.

- `adsic`: averaged density self-interaction correction.

- `rhoxc`: density correction to the exchange.

### KEDFunctional
##### Type: integer
##### Default: none

Kinetic energy density functional. This is only used when running the `xc`
calculation mode. For a complete list of available functionals, check the Libxc
documentation.

### TheoryLevel
##### Type: integer
##### Default: dft

The possible values are:

- `independent_particles`: particles are considered as independent, i.e. as
  non-interacting.

- `dft`: this is the default density-functional theory scheme.

### Xalpha
##### Type: real
##### Default: 1.0

Sets the value of the alpha parameter when using Slater’s Xalpha functional
(`lda_c_xalpha`).

### TB_c
##### Type: real
##### Default: 1.0

Fixes the value of the c parameter to a given value when using the Tran-Blaha
functional (`mgga_x_tb09`). If the value is set to 1.0, then the parameter is
calculated in the usual way. Note that c=1.0 gives the Becke & Johnson 06
functional (`mgga_x_bj06`).

## Species


### NuclearCharge
##### Type: real

Nuclear charge of the species.

### Orbitals
##### Type: block data

This options sets the electronic configuration of the species. To each line
corresponds an orbital, which is defined by some quantum numbers and its
occupancy.

The first and second columns are the main quantum number $n$ and the angular
momentum quantum number $l$. The meaning of the remaining columns depends if we
are running a fully-relativistic spin-polarized calculation or not. In the later
case, the remaining columns are used to specify the occupancies. One or two
columns can be used to specify the occupancies and there are four possible
cases:

- The calculation is spin-unpolarized and only the third column is set: the
  occupancy of the orbital is simply set by the third column.

- The calculation is spin-unpolarized and the third and fourth columns are set:
  the occupancy of the orbital is equal to the sum of both columns.

- The calculation is spin-polarized and only the third column is set: the
  occupancies of the two spin channels are equal to half the value of the third
  column.

- The calculation is spin-polarized and the third and fourth columns are set:
  the occupancies of the two spin channels are set by the two columns.

When running a fully-relativistic spin-polarized calculation (`WaveEquation =
dirac` and `SpinMode = polarized`), besides the $n$ and $l$ quantum numbers, it
is also necessary to specify the $m$ quantum number, which will always be the
third column of the block. This quantum number runs from $-j$ to $j$, where $j$
can have two values: $j = l - 1/2$ and $j = l + 1/2$. This means that, when $|m|
\lt l+1/2$ there will be two orbitals with the same $m$ quantum number. To
differentiate these two orbitals, a fourth quantum number is introduced, which
can have two values: *up* or *down* (these are represented in APE by $0.5$ and
$-0.5$). The meaning of the columns beyond the third is thus the following:

- If $|m| = 2l+1$: the fourth column is just the occupancy.

- If $|m| \lt 2l+1$: the fourth column is the occupancy of the *up* orbital and
  the fifth column is the occupancy of the *down* orbital.  If only the fourth
  column is set, both orbitals are equally occupied with half the specified
  value.

Note that the occupancies can be set to zero.

Here is an example for the Lithium ground state electronic
configuration:
```no-lang
 %Orbitals
  1 | 0 | 1 | 1
  2 | 0 | 1 | 0
  2 | 1 | 0 | 0
%
```

The first column sets the quantum number $n$, the second the quantum number $l$
and the last two the occupancies for both spin channels. In case of a
fully-relativistic spin-polarized calculation, the previous example would
become:

```no-lang
%Orbitals
  1 | 0 | -1/2 | 1
  1 | 0 |  1/2 | 1
  2 | 0 | -1/2 | 1
  2 | 0 |  1/2 | 0
  2 | 1 | -3/2 | 0
  2 | 1 | -1/2 | 0 | 0
  2 | 1 |  1/2 | 0 | 0
  2 | 1 |  3/2 | 0
%
```

To make things easier, the core configuration can be replaced by a string with
the chemical symbol of the rare gas that has the same configuration. For
example, to run Sodium, it is possible to use the following input:

```no-lang
%Orbitals
  "Ne"
  3 | 0 | 1
%
```



## Pseudopotentials generation

### PPComponents
##### Type: block data

This block sets which pseudopotential components will be generated and the
corresponding core radii. The first column is the main quantum number $n$, the
second column is the angular momentum quantum number $l$, the one before the
last is the core radius, and the last is the scheme to be used for constructing
the pseudopotentials.

When generating relativistic pseudopotentials, the third column will be the
total angular momentum quantum number $j$. Nevertheless, it is possible to omit
$j$, in which case both $j=l-1/2$ and $j=l+1/2$ components will use the same
core radius.

Here is an example:
```no-lang
%PPComponents
  5 | 0 | 1.0 | tm
  5 | 1 | 0.5 | 1.4 | tm
  5 | 1 | 1.5 | 1.6 | tm
  4 | 2 | 2.0 | tm
%
```

In this case, the $j=l-1/2$ and $j=l+1/2$ components of $l=1$ will have
different core radius, while for $l=2$ they will have the same core radius.

Note that it is possible to set the core-radii to zero. In that case, APE will
use some default value that depends on which scheme is used for constructing the
pseudopotentials. Right now, this feature only works with the Hamann scheme. In
that case the default core-radius is determined using the value of the outermost
maximum of the all-electron wavefunction. If there are core states present with
the same angular momentum the default core-radius is equal to 0.6 times the
outermost maximum. Otherwise, the core radius is 0.4 times the outermost
maximum.

The possible values for the scheme used for constructing the
pseudopotentials are:

- `ham`: Hamann scheme.

- `tm`: Troullier-Martins scheme.

- `rtm`: Relativistic extension of the Troullier-Martins scheme.

- `mrpp`: MRPP scheme.

Note that when using the MRPP scheme, the quantum numbers should correspond to a
semi-core orbital, i.e., there should be at least another orbital with the same
$l$ and $j$ quantum numbers, but higher in energy.

### PPCalculationTolerance
##### Type: real
##### Default: 1e-6

This tolerance is used during the pseudopotential generation. What it does
exactly depends on the pseudopotential generation scheme used.

### PPOutputFileFormat
##### Type: integer
##### Default: upf

APE can write the pseudopotentials in different formats compatible with other
programs. Programs currently supported are:

- `siesta`:
    [SIESTA](https://departments.icmab.es/leem/siesta/)
    format.

- `fhi`: [fhi98PP](http://www.fhi-berlin.mpg.de/th/fhi98md/fhi98PP/)
    pseudopotential generator format.

- `abinit5`: [ABINIT](http://www.abinit.org) format 5.

- `abinit6`: [ABINIT](http://www.abinit.org) format 6.

- `upf`: [Quantum-Espresso](http://www.quantum-espresso.org) unified pseudopotential format.

- `parsec`: [parsec](http://parsec.ices.utexas.edu) format. This is basically
  the SIESTA format plus the pseudo-wavefunctions.

- `latepp_so` : LATEPP format. Spin-orbit difference potentials and respective
  wavefunctions for the spin-orbit treatment in the LATEPP code.

### CoreCorrection
##### Type: integer
##### Default: 1

Scheme for constructing the partial core density. Possible values are:

- `0`: no non-linear core corrections.

- `1`: scheme implemented in José Luis Martins atom code.

- `2`: scheme implemented in the fhi98PP code.

### LLocal
##### Type: integer
##### Default: -1

Angular momentum component of the pseudo-potential to take as local component
when building the Kleinman-Bylander projectors. If set to -1 a Vanderbilt
function is used.



## Pseudopotentials tests


### PPTests
##### Type: integer
##### Default: ld+dm

What tests should be performed. Possible values are:

- `ld`: logarithmic derivatives.

- `dm`: transition dipole moments.

- `ip_test`: ionization potentials.

### PPTestSCF
##### Type: logical
##### Default: false

If set to `yes`, run a SCF calculation for the pseudostates before performing
the tests.

### PPTestOrbitals
##### Type: block data
##### Default:

Orbitals configuration to be used for the SCF calculation performed when
PPTestSCF is set ot `yes`. This allows to change the occupancies of the valence
states or to add other states to be tested.

### PPTestAEDir
##### Type: string
##### Default: "ae"

Directory where to find the all-electron calculations that should be used during
the tests. The default is the `ae` directory.

### LogDerivativeRadius
##### Type: real
##### Default: 0.0

Diagnostic radius where the logarithmic derivatives are computed. If set to
zero, APE will use the element covalent radius.

### LogDerivativeEnergyMax
##### Type: real

Upper bound of the energy interval where the logarithmic derivatives are
computed. If it is not set, then the value to be used will be equal to the
largest eigenvalue plus 1 a.u.

### LogDerivativeEnergyMin
##### Type: real

Lower bound of the energy interval where the logarithmic derivatives are
computed. If it is not set, then the value to be used will be equal to the
smallest eigenvalue minus 1 a.u.

### LogDerivativeEnergyStep
##### Type: real
##### Default: 0.0 a.u.

Step in energy interval. If set to zero, an adaptive step will be used.



## Mesh


APE uses a mesh to store functions. In order to change the mesh parameters you
can use the following options:

### MeshType
##### Type: integer
##### Default: log1

Possible values are:

- `lin`: Linear

- `log1`: Logarithmic, defined as $r_i = b \, e^{a \, i}$

- `log2`: Logarithmic, defined as $r_i = b \; (e^{a \, i} - 1)$

### MeshStartingPoint
##### Type: real
##### Default: sqrt(NuclearCharge)*1e-5 a.u.

Sets the starting point of the mesh.

### MeshOutmostPoint
##### Type: real
##### Default: sqrt(NuclearCharge)*30 a.u.

Sets the outmost point of the mesh.

### MeshNumberOfPoints
##### Type: integer
##### Default: sqrt(NuclearCharge)*200

Sets the mesh number of points.

### MeshDerivMethod
##### Type: integer
##### Default: cubic_spline

Numerical method used to compute derivatives. Possible choices are:

- `cubic_spline`: Use cubic splines to interpolate the function and then uses
  this interpolation to compute the derivatives.

- `finite_diff`: Finite differences. The number of points used is controled by
  the `MeshFiniteDiffOrder` variable.

### MeshFiniteDiffOrder
##### Type: integer
##### Default: 4

This variable controls the numbers of points used for the finite differences
discretization. To compute the derivative at a given point, the finite
difference formula will use `MeshFiniteDiffOrder` points to the left and
`MeshFiniteDiffOrder` points to the right. This means that in total
`MeshFiniteDiffOrder`$*2 + 1$ points will be used.


## SCF


The self consistent field procedure will stop when one of the convergence
criteria is fulfilled. At each iteration the new guess potential is built mixing
the input and output potentials.

### MaximumIter
##### Type: integer
##### Default: 300

Maximum number of SCF iterations. When that number is reached the SCF procedure
will stop, even if none of the criteria are fulfilled, and the calculation will
continue as normal. `0` means unlimited.

### ConvAbsDens
##### Type: real
##### Default: 0.0

Absolute convergence of the density. `0` means do not use this
criterion.

### ConvRelDens
##### Type: real
##### Default: 1e-8

Relative convergence of the density. `0` means do not use this
criterion.

### ConvAbsEnergy
##### Type: real
##### Default: 0.0

Absolute convergence of the total energy. `0` means do not use this
criterion.

### ConvRelEnergy
##### Type: real
##### Default: 0.0

Relative convergence of the total energy. `0` means do not use this
criterion.

### SmearingFunction
##### Type: integer
##### Default: fixed_occ

Select how the states should be occupied. The option are:

- `fixed_occ`: the occupancies of the states are fixed.

- `semiconducting`: semiconducting occupancies, i.e., the lowest lying states
  are occupied until no more electrons are left

- `averill_painter`: F.W. Averill and G.S. Painter, Phys. Rev. B 46, 2498 (1992)

### MixingScheme
##### Type: integer
##### Default: broyden

Selects the mixing procedure to be used during the SCF cycle. Possible
values are:

- `linear`: Linear mixing.

- `broyden`: Broyden mixing.

### Mixing
##### Type: real
##### Default: 0.3

Determines the amount of the new potential which is to be mixed with the
old one.

### MixingSteps
##### Type: integer
##### Default: 3

Number of steps used by Broyden mixing to extrapolate the new potential.


## Wave-equations solver


APE solves the Schrodinger and Dirac equations using the ODE solver
from gsl.

### EigenSolverTolerance
##### Type: real
##### Default: 1e-8 a.u.

The eigensolver will improve the eigenvalues untill the error estimate of each
eigenvalue is smaller than this tolerance.

### ODEIntTolerance
##### Type: real
##### Default: 1e-12

### ODESteppingFunction
##### Type: integer
##### Default: rkpd8

Possible values are:

- `rk2`: Embedded 2nd order Runge-Kutta method

- `rk4`: 4th order (classical) Runge-Kutta method

- `rkf4`: Embedded 4th order Runge-Kutta-Fehlberg method

- `rkck4`: Embedded 4th order Runge-Kutta Cash-Karp method

- `rkpd8`: Embedded 8th order Runge-Kutta Prince-Dormand method

For more information about the stepping functions please refer to the gsl
documentation.

### ODEMaxSteps
##### Type: integer
##### Default: 500000

Sometimes it may happen that the ODE solver takes too many steps, because of
some problem. To avoid that, APE will issue a fatal error if the number of steps
is greater than `ODEMaxSteps`.
